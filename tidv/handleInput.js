const axiosRequest = require('../app/axiosRequest');
const promptHandler = require('./promptHandler');
const processToken = require('./processToken');
const log = require('./log');

function dataToPost(authId, prompt, value) {
  return {
    authId,
    callbacks: [
      {
        type: 'NameCallback',
        output: [
          {
            name: 'prompt',
            value: prompt,
          },
        ],
        input: [
          {
            name: 'IDToken1',
            value,
          },
        ],
        _id: 0,
      },
    ],
  };
}

function getValue(body) {
  const { prompt } = body;
  if (prompt === 'Date of Birth') {
    return body.dob;
  }
  if (prompt === 'Phone Number') {
    return body.phone;
  }
  if (prompt === 'Postcode') {
    return body.postcode;
  }
  if (prompt === 'Enter NINO') {
    return body.nino;
  }
  if (prompt.startsWith('{"')) { // JSON prompt
    return body.kbv === 'true';
  }
  return '';
}

function makeValue(outcome) {
  const data = {
    outcome,
    inputType: 'voice',
    attemptCount: 1,
    confirmed: true,
  };
  return JSON.stringify(data);
}

async function handlePost(req, res) {
  log('INPUT', req.body);
  const {
    authId,
    authUrl,
    sessionId,
  } = req.body;
  const value = getValue(req.body);
  const data = dataToPost(authId, req.body.prompt, makeValue(value));
  log('SENDING', value, JSON.stringify(data));
  const response = await axiosRequest({
    url: authUrl,
    data,
    headers: {
      'session-id': sessionId,
    },
  });
  log('RESPONSE', response.status, JSON.stringify(response.data));
  if (response.status !== 200) {
    res.send(response.data);
    return;
  }
  if (response.data.tokenId) {
    log('RESPONSE_HEADERS', response.headers);
    const success = await processToken(response.data, authUrl, req.body);
    res.send(success);
    return;
  }
  const nextPrompt = response.data.callbacks[0].output[0].value;
  res.send(promptHandler(nextPrompt, req.body));
}

module.exports = (app) => {
  app.post('/input', handlePost);
};
