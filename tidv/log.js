function log(tag, ...args) {
  // eslint-disable-next-line no-console
  console.log(`\x1b[35m${tag}\x1b[0m`, ...args);
}

module.exports = log;
