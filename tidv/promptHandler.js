const enterDateOfBirth = require('./enterDateOfBirth');
const enterPhoneNumber = require('./enterPhoneNumber');
const enterPostcode = require('./enterPostcode');
const enterNINO = require('./enterNINO');
const enterKBV = require('./enterKBV');

const map = {
  'Date of Birth': enterDateOfBirth,
  'Phone Number': enterPhoneNumber,
  Postcode: enterPostcode,
  'Enter NINO': enterNINO,
  // cis_home_phone: enterKBV
};

function promptHandler(prompt, data) {
  const fn = map[prompt];
  if (fn) {
    return fn(data);
  }
  return enterKBV({
    ...data,
    prompt,
  });
  // return `Unknown Prompt: ${prompt}`;
}

module.exports = promptHandler;
