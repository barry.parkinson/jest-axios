const escape = require('./escape');
const getCSS = require('./getCSS');

function enterPostcode({
  line,
  cli,
  authId,
  authUrl,
  dob,
  phone,
  sessionId,
}) {
  return `<html lang="en">
<head>
<title>TIDV Enter Post Code</title>
${getCSS()}
</head>
<body>
<h1>TIDV Test App - Enter Post Code</h1>
<p>${line} ${escape(cli)} ${escape(dob)}</p>
<form method="post" action="/input" onsubmit="clickButton()">
<input type="hidden" name="line" value="${line}">
<input type="hidden" name="cli" value="${escape(cli)}">
<input type="hidden" name="dob" value="${escape(dob)}">
<input type="hidden" name="phone" value="${escape(phone)}">
<input type="hidden" name="authId" value="${authId}">
<input type="hidden" name="authUrl" value="${authUrl}">
<input type="hidden" name="sessionId" value="${sessionId}">
<input type="hidden" name="prompt" value="Postcode">
<p>Enter the post code:</p>
<input type="text" name="postcode">
<button type="submit" id="theButton">Done</button>
</form>
</body>
</html>`;
}

module.exports = enterPostcode;
