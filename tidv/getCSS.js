function getCSS() {
  return `<style>
* {
  font-family: Arial, sans-serif;
}
code {
  white-space: pre;
  font-family: monospace;
}
select,
button,
input[type=text] {
  padding: 8px;
}
select,
button {
  min-width: 100px;
}
</style>
<script>
function clickButton() {
  let b = document.getElementById("theButton");
  b.disabled = true;
  b.innerText = "Sending...";
  b.textContent = "Sending...";
  return true;
}
</script>
`;
}

module.exports = getCSS;
