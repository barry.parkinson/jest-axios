const jwt = require('jsonwebtoken');
const escape = require('./escape');
const getCSS = require('./getCSS');

function safe(s) {
  return escape(JSON.stringify(s, undefined, ' '));
}

function successPage(
  ssoData,
  tokenData,
  introspectData,
  userInfo,
  paymentData,
  idvData,
  sessionData,
) {
  const jwtData = jwt.decode(tokenData.id_token, { complete: true });

  return `<html lang="en">
<head>
<title>TIDV Complete</title>
${getCSS()}
</head>
<body>
<h1>TIDV Test App - Success</h1>

<h3>SSO Token</h3>
<code>${safe(ssoData)}</code>

<h3>Access Token Data</h3>
<code>${safe(tokenData)}</code>

<h3>JWT id_token</h3>
<code>${safe(jwtData.header)}</code>
<code>${safe(jwtData.payload)}</code>

<h3>Introspect</h3>
<code>${safe(introspectData)}</code>

<h3>Userinfo</h3>
<code>${safe(userInfo)}</code>

<h3>Payment Data</h3>
<code>${safe(paymentData)}</code>

<h3>IDV Failure Service</h3>
<code>${safe(idvData)}</code>

<h3>Session Data</h3>
<code>${safe(sessionData)}</code>

</body>
</html>`;
}

module.exports = successPage;
