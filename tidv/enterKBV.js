const escape = require('./escape');
const getCSS = require('./getCSS');

function enterKBV({
  line,
  cli,
  authId,
  authUrl,
  dob,
  prompt,
  sessionId,
  phone,
  postcode,
  nino,
}) {
  return `<html lang="en">
<head>
<title>TIDV KBV</title>
${getCSS()}
</head>
<body>
<h1>TIDV Test App - KBV</h1>
<p>${line} ${escape(cli)} ${escape(dob)}</p>
<p>${escape(prompt)}</p>
<form method="post" action="/input" onsubmit="clickButton()">
<input type="hidden" name="sessionId" value="${sessionId}">
<input type="hidden" name="line" value="${line}">
<input type="hidden" name="cli" value="${escape(cli)}">
<input type="hidden" name="dob" value="${escape(dob)}">
<input type="hidden" name="phone" value="${escape(phone)}">
<input type="hidden" name="postcode" value="${escape(postcode)}">
<input type="hidden" name="nino" value="${escape(nino)}">
<input type="hidden" name="authId" value="${authId}">
<input type="hidden" name="authUrl" value="${authUrl}">
<input type="hidden" name="prompt" value="${escape(prompt)}">
<p>Enter the KBV:</p>
<select name="kbv" id="kbv">
  <option value="true">Pass</option>
  <option value="false">Fail</option>
</select>
<button type="submit" id="theButton">Done</button>
</form>
</body>
</html>`;
}

module.exports = enterKBV;
