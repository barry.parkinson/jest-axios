const crypto = require('crypto');
const axiosRequest = require('../app/axiosRequest');
const enterDateOfBirth = require('./enterDateOfBirth');
const getCSS = require('./getCSS');
const log = require('./log');

const page = `<html lang="en">
<head>
<title>TIDV Enter CLI</title>
${getCSS()}
</head>
<body>
<h1>TIDV Test App - Enter CLI</h1>
<form method="post" action="/start" onsubmit="clickButton()">
<label for="line">Select source line:</label>
<select name="line" id="line">
  <option value="CxP-PIP-TIDV">PIP</option>
  <option value="CxP-ESA-TIDV">ESA</option>
</select>
<p>Enter the CLI phone number:</p>
<input type="text" name="cli">
<button type="submit" id="theButton">Start Call</button>
</form>
</body>
</html>`;

function homePage(req, res) {
  res.send(page);
}

const map = {
  'CxP-PIP-TIDV': process.env.PIP_URL,
  'CxP-ESA-TIDV': process.env.ESA_URL,
};

function makeValue(outcome) {
  const data = {
    outcome,
    inputType: 'voice',
    attemptCount: 1,
    confirmed: true,
  };
  return JSON.stringify(data);
}

function error(response) {
  return {
    status: response.status,
    data: response.data,
    headers: response.headers,
  };
}

async function homePost(req, res) {
  const {
    cli,
    line,
  } = req.body;
  const sessionId = crypto.randomUUID();
  log('SESSION_ID', sessionId);

  // GET THE URL FOR THE PAYMENT SERVICE - KONG WILL REDIRECT to /authorize
  const url = map[line];
  const response = await axiosRequest({
    url,
    method: 'get',
  });
  if (response.status !== 302) {
    res.send(error(response));
    return;
  }
  log('START', line, cli, url, response.status, response.headers.location);

  // GET THE /authorize URL - THIS WILL REDIRECT TO /authenticate (THE TIDV MICROSERVICE)
  const auth = await axiosRequest({
    url: response.headers.location,
    method: 'get',
  });
  log('AUTHZ', auth.status, auth.headers.location);

  // POST TO THE /authenticate URL
  const authenticate = auth.headers.location;
  const initial = await axiosRequest({
    url: authenticate,
    data: {},
    headers: {
      'session-id': sessionId,
    },
  });
  log('INITIAL', initial.status, JSON.stringify(initial.data));
  if (initial.status !== 200) {
    res.send(error(initial));
    return;
  }

  // POST THE cli NUMBER
  initial.data.callbacks[0].input[0].value = makeValue(cli);
  const afterCLI = await axiosRequest({
    url: authenticate,
    data: initial.data,
    headers: {
      'session-id': sessionId,
    },
  });
  log('AFTER_CLI', afterCLI.status, JSON.stringify(afterCLI.data));
  if (afterCLI.status !== 200) {
    res.send(error(afterCLI));
    return;
  }
  // const prompt = dob.data.callbacks[0].output[0].value;

  // RETURN PAGE TO ENTER DATE OF BIRTH
  res.send(enterDateOfBirth({
    line,
    cli,
    authId: afterCLI.data.authId,
    authUrl: authenticate,
    sessionId,
  }));
}

module.exports = (app) => {
  app.get('/', homePage);
  app.post('/start', homePost);
};
