const os = require('os');
const axiosRequest = require('../app/axiosRequest');
const successPage = require('./successPage');
const log = require('./log');

function extractParam(paramName, location) {
  if (location) {
    const i = location.indexOf('?');
    const params = location.substring(i + 1);
    const arr = params.split('&');
    const theParam = arr.find((p) => p.startsWith(`${paramName}=`));
    if (theParam) {
      const encoded = theParam.substring(paramName.length + 1);
      return decodeURIComponent(encoded);
    }
  }
  return undefined;
}

async function processToken(ssoData, authUrl, sessionData) {
  const { tokenId } = ssoData;
  const gotoUrl = extractParam('goto', authUrl);
  log('GOTO', gotoUrl);

  // POST TO THE /authorize END POINT TO GET THE code URL
  const authResponse = await axiosRequest({
    url: gotoUrl,
    method: 'post',
    data: `decision=allow&csrf=${encodeURIComponent(tokenId)}`,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      cookie: `DTH_AaaS_Session=${encodeURIComponent(tokenId)}`,
      // 'session-id': sessionId,
      'user-agent': `${os.hostname()}/authorize`,
    },
  });
  const { location } = authResponse.headers;
  log('AUTHORIZE', authResponse.status, location);
  sessionData['(code-location)'] = location; // eslint-disable-line no-param-reassign

  // THIS JUST REDIRECTS TO /authorize
  // const getResponse = await axiosRequest({
  //   url: location,
  //   method: 'get',
  // });
  // log('GET', getResponse.status, getResponse.data, getResponse.headers);

  const code = extractParam('code', location);
  // const state = extractParam('state', location);
  // const nonce = extractParam('nonce', location);
  const clientId = extractParam('client_id', location);
  const redirectUri = extractParam('redirect_uri', gotoUrl);
  const iss = extractParam('iss', location);

  const secrets = {
    'CxP-PIP-TIDV': process.env.PIP_SECRET,
    'CxP-ESA-TIDV': process.env.ESA_SECRET,
  };
  const secret = secrets[clientId];

  const codeData = [
    'grant_type=authorization_code',
    `&client_id=${clientId}`,
    `&client_secret=${encodeURIComponent(secret)}`, // ForgeRock requires this!
    `&code=${encodeURIComponent(code)}`,
    `&redirect_uri=${encodeURIComponent(redirectUri)}`,
  ].join('');
  log('SEND_CODE', iss, codeData);

  // GET THE ACCESS TOKEN AND THE JWT id_token
  const tokenResponse = await axiosRequest({
    url: `${iss}/access_token`,
    data: codeData,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'user-agent': `${os.hostname()}/authorization_code`,
    },
  });
  log('TOKEN', tokenResponse.status, tokenResponse.data); // , tokenResponse.headers);

  // CALL INTROSPECT
  const introResponse = await axiosRequest({
    url: `${iss}/introspect`,
    data: [
      `client_id=${clientId}`,
      `&client_secret=${encodeURIComponent(secret)}`,
      `&token=${encodeURIComponent(tokenResponse.data.access_token)}`,
    ].join(''),
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'user-agent': `${os.hostname()}/introspect`,
    },
  });

  // CALL USERINFO
  const userInfoResponse = await axiosRequest({
    url: `${iss}/userinfo`,
    method: 'get',
    headers: {
      authorization: `Bearer ${tokenResponse.data.access_token}`,
      'user-agent': `${os.hostname()}/userinfo`,
    },
  });
  log('USERINFO', userInfoResponse.status, userInfoResponse.data);

  // CALL PAYMENT SERVICE
  const paymentResponse = await axiosRequest({
    url: redirectUri,
    method: 'get',
    headers: {
      'x-access-token': tokenResponse.data.access_token,
      correlationId: sessionData.sessionId,
    },
  });
  log('GET_PAYMENT', paymentResponse.status, paymentResponse.data, paymentResponse.headers);
  const paymentData = {
    status: paymentResponse.status,
    ...paymentResponse.data,
    guid: paymentResponse.headers.guid,
    url: redirectUri,
  };

  // FAILURE REASON SERVICE
  const { guid } = introResponse.data;
  const raw = JSON.stringify({ guid });
  const idvResponse = await axiosRequest({
    url: process.env.IDV_FAILURE_URL,
    method: 'get',
    headers: {
      'x-access-token': tokenResponse.data.access_token,
      'session-id': sessionData.sessionId,
      // correlationId: sessionData.sessionId,
      // 'x-userinfo': Buffer.from(raw).toString('base64'),
    },
  });
  log('x-userinfo', Buffer.from(raw)
    .toString('base64'));
  log('IDV_FAILURE', idvResponse.status, idvResponse.data, idvResponse.headers);

  // DONE
  return successPage(
    ssoData,
    tokenResponse.data,
    introResponse.data,
    userInfoResponse.data,
    paymentData,
    idvResponse.data,
    sessionData,
  );
}

module.exports = processToken;
