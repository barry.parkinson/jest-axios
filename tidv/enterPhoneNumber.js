const escape = require('./escape');
const getCSS = require('./getCSS');

function enterPhone({
  line,
  cli,
  authId,
  authUrl,
  dob,
  postcode,
  sessionId,
}) {
  return `<html lang="en">
<head>
<title>TIDV Enter Phone Number</title>
${getCSS()}
</head>
<body>
<h1>TIDV Test App - Enter Phone Number</h1>
<p>${line} ${escape(cli)} ${escape(dob)}</p>
<form method="post" action="/input" onsubmit="clickButton()">
<input type="hidden" name="line" value="${line}">
<input type="hidden" name="cli" value="${escape(cli)}">
<input type="hidden" name="dob" value="${escape(dob)}">
<input type="hidden" name="postcode" value="${escape(postcode)}">
<input type="hidden" name="authId" value="${authId}">
<input type="hidden" name="authUrl" value="${authUrl}">
<input type="hidden" name="sessionId" value="${sessionId}">
<input type="hidden" name="prompt" value="Phone Number">
<p>Enter the phone number:</p>
<input type="text" name="phone">
<button type="submit" id="theButton">Done</button>
</form>
</body>
</html>`;
}

module.exports = enterPhone;
