const { replace } = '';

const regExp = /[&<>'"]/g;

const replacements = {
  '&': '&amp;',
  '<': '&lt;',
  '>': '&gt;',
  '\'': '&#39;',
  '"': '&quot;',
};

const replacer = (m) => replacements[m];

/**
 * Escape HTML characters ie "& <" to "&amp; &lt;".
 * @param str String containing unsafe characters.
 * @returns {string} Escaped string.
 */
module.exports = (str) => ((str !== undefined && str !== null) ? replace.call(str, regExp, replacer) : '');
