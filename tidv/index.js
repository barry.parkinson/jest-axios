const express = require('express');
const log = require('./log');

const app = express();

app.set('x-powered-by', false);
app.set('etag', false);

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

require('./home')(app);
require('./handleInput')(app);

// start service
const port = process.env.PORT || 4039;

app.listen(port, () => {
  log(`TIDV test app running on ${port}`);
});

module.exports = app;
