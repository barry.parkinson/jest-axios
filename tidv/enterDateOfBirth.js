const escape = require('./escape');
const getCSS = require('./getCSS');

function dobPage({
  line,
  cli,
  authId,
  authUrl,
  sessionId,
}) {
  return `<html lang="en">
<head>
<title>TIDV Enter Date of Birth</title>
${getCSS()}
</head>
<body>
<h1>TIDV Test App - Date of Birth</h1>
<p>${line} ${escape(cli)}</p>
<form method="post" action="/input" onsubmit="clickButton()">
<input type="hidden" name="line" value="${line}">
<input type="hidden" name="cli" value="${escape(cli)}">
<input type="hidden" name="authId" value="${authId}">
<input type="hidden" name="authUrl" value="${authUrl}">
<input type="hidden" name="sessionId" value="${sessionId}">
<input type="hidden" name="prompt" value="Date of Birth">
<p>Enter the date of birth:</p>
<input type="text" name="dob">
<button type="submit" id="theButton">Done</button>
</form>
</body>
</html>`;
}

module.exports = dobPage;
