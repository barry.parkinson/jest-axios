const escape = require('./escape');
const getCSS = require('./getCSS');

function enterNINO({
  line,
  cli,
  authId,
  authUrl,
  dob,
  postcode,
  phone,
  sessionId,
}) {
  return `<html lang="en">
<head>
<title>TIDV Enter NINO</title>
${getCSS()}
</head>
<body>
<h1>TIDV Test App - Enter NINO</h1>
<p>${line} ${escape(cli)} ${escape(dob)}</p>
<form method="post" action="/input">
<input type="hidden" name="line" value="${line}" onsubmit="clickButton()">
<input type="hidden" name="cli" value="${escape(cli)}">
<input type="hidden" name="dob" value="${escape(dob)}">
<input type="hidden" name="phone" value="${escape(phone)}">
<input type="hidden" name="postcode" value="${escape(postcode)}">
<input type="hidden" name="authId" value="${authId}">
<input type="hidden" name="authUrl" value="${authUrl}">
<input type="hidden" name="sessionId" value="${sessionId}">
<input type="hidden" name="prompt" value="Enter NINO">
<p>Enter the NINO:</p>
<input type="text" name="nino">
<button type="submit" id="theButton">Done</button>
</form>
</body>
</html>`;
}

module.exports = enterNINO;
