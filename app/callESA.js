const { version } = require('../package.json');
const logAPIResponse = require('./logAPIResponse');
const axiosRequest = require('./axiosRequest');

const headers = (channel) => {
  let userAgent = `DTH-KBV/${version}`;
  if (channel === 'phone') {
    userAgent = 'DthKnowledgeBasedVerification (NextPaymentDetails)';
  } else if (channel === 'web') {
    userAgent = 'DthKnowledgeBasedVerification';
  }
  return {
    'Content-Type': 'application/json',
    'User-Agent': userAgent,
    'X-Api-Key': process.env.IAG_API_KEY,
  };
};

// api can be either 'request' or 'retrieve'
// timeout is irrelevant for 'request'
// channel is not needed for 'retrieve'

async function callESA(req, api, nino, channel = undefined) {
  const response = await axiosRequest({
    url: `${process.env.IAG_URL.replace(/\/+$/, '')}/${api}`,
    method: 'POST',
    headers: headers(channel),
    timeout: process.env.IAG_REQUEST_TIMEOUT,
    data: {
      nino: nino.toUpperCase(),
    },
  });
  logAPIResponse(req, `esa-${api}`, response);
  return response;
}

module.exports = callESA;
