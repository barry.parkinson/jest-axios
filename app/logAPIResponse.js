// log and meter

/**
 * Log and Meter an axios response received when calling a downstream service
 * @param req the express request
 * @param api string ie 'esa-request' or 'cis-455'
 * @param response the response received from axios
 */

function logAPIResponse(req, api, response) {
  const { status } = response;
  const isError = !(status >= 200 && status <= 204); // TODO what is an error depends on API

  const log = isError ? req.logger.error : req.logger.info;

  log({
    api,
    status,
    request_time: response.requestTime,
    // headers: response.headers,
    data: isError ? response.data : undefined,
  });
}

module.exports = logAPIResponse;
