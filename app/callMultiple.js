const callCIS = require('./callCIS');
const callPIP = require('./callPIP');
const callESA = require('./callESA');

function log(tag, ...args) {
  process.stdout.write(`\x1b[36m${tag}\x1b[0m ${args.join(' ')}\n`);
}

async function callMultiple(req, nino, isESALine) {
  const response = await callCIS(req, 'api-014', nino, true);
  const { data } = response;
  if (data.getFullAwardHistoryResponse) {
    const { personRoleServiceAssmtMap } = data.getFullAwardHistoryResponse;
    const ids = personRoleServiceAssmtMap.map((award) => award.dwpServiceKeyID);
    let PIP = false;
    if (ids.includes('675')) {
      PIP = true;
      const pips = await Promise.allSettled([
        callPIP(req, 'PIPGetPersonalNewDetails', nino),
        callPIP(req, 'PIPGetPaymentDetails', nino),
        callPIP(req, 'PIPGetEntitlement', nino),
      ]);
      pips.forEach((result) => {
        const res = result.value;
        log('PIP MULTI', result.status, res.status); // res.data is xml
      });
    }
    if (ids.includes('85') && (isESALine || PIP === false)) {
      log('ESA');
      callESA(req, 'request', nino, 'phone')
        .then(() => undefined);
    }
  }
  return response;
}

module.exports = callMultiple;
