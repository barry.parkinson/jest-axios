const callESA = require('./callESA');

const BANK_PREFIX = 'bank';
const BANK_ACCOUNT_MATCH_PATTERN = /\d{8}/g;
const SORT_CODE_MATCH_PATTERN = /\d{2}-\d{2}-\d{2}/g;
const BUILDING_SOCIETY_PREFIX = 'bsoc';
const BUILDING_SOCIETY_ACCOUNT_MATCH_PATTERN = /BSoc acc No: (.*) BSoc code:/i;

async function waitFor(t) {
  return new Promise((resolve) => {
    setTimeout(() => resolve(), t);
  });
}

async function queryESA(req, nino, tries = 1) {
  const res = await callESA(req, 'retrieve', nino);
  if (res.status === 202) {
    if (tries >= 5) {
      return res;
    }
    await waitFor(1000);
    return queryESA(req, nino, tries + 1);
  }
  // console.log(res.data);
  const {
    accountDetails = '',
    lastPaid,
    amount,
    bwe,
  } = res.data;
  const q = [];
  // esa_bank_account and esa_sort_code
  const accountType = accountDetails.toLowerCase();
  if (accountType.startsWith(BANK_PREFIX)) {
    const accountMatch = accountDetails.match(BANK_ACCOUNT_MATCH_PATTERN);
    if (accountMatch) {
      const account = accountMatch[0];
      q.push({
        q: 'esa_bank_account',
        a: account.substring(account.length - 4),
      });
    }
    const sortCodeMatch = accountDetails.match(SORT_CODE_MATCH_PATTERN);
    if (sortCodeMatch) {
      q.push({
        q: 'esa_sort_code',
        a: sortCodeMatch[0].replace(/-/g, ''),
      });
    }
  } else if (accountType.startsWith(BUILDING_SOCIETY_PREFIX)) {
    const match = accountDetails.match(BUILDING_SOCIETY_ACCOUNT_MATCH_PATTERN);
    if (match) {
      const accountString = match[1].trim();
      q.push({
        q: 'esa_bank_account',
        a: accountString.substring(accountString.length - 4),
      });
    }
  }
  // others
  if (amount) {
    q.push({
      q: 'esa_last_payment_amount',
      a: String(Math.round(amount * 100)),
    });
  }
  if (lastPaid) {
    q.push({
      q: 'esa_last_payment_date',
      a: lastPaid, // TODO format 19/11/20 to iso
    });
  }
  if (bwe) {
    q.push({
      q: 'esa_pay_day',
      a: bwe,
    });
  }
  console.log(res.status, q);
  return res;
}

module.exports = queryESA;

// accountDetails: 'Bank acc No: 12349876 Bank sort code: 40-47-84',
// "accountDetails": "BSoc acc No: 812319823u9123ABC1  BSoc code: MW"
