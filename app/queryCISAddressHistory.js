const callCIS = require('./callCIS');

async function queryCISAddressHistory(req, ninoQuery) {
  const res = await callCIS(req, 'api-007', ninoQuery, true);
  return res;
}

module.exports = queryCISAddressHistory;
