const axios = require('axios');

// see documentation here
// https://test.exchange.dwp.gov.uk/portal/documentation/digital-payments-services-exchange-payment-dates-api

async function go() {
  // const url = 'https://pd-dev-api.pd-dev.eng.payments-dev-future.dwpcloud.uk/payment-dates/dates/2024-12-10?region=england-and-wales';
  const url = 'https://pd-test-api.pd-test.eng.payments-dev-future.dwpcloud.uk/payment-dates/dates/2024-12-10?region=england-and-wales';
  // test environment
  // const url = 'https://fpay-test-exchange.payments-dev-future.dwpcloud.uk:8443/payment-dates/dates/2024-12-27?region=united-kingdom';
  // test environment from HCS-EKS (with istio handling mTLS
  // const url = 'https://fpay-test-exchange.payments-dev-future.dwpcloud.uk:8443/payment-dates/dates/2024-12-27?region=united-kingdom';
  // production
  // const url = 'https://fpay-prod-exchange.payments-prod-future.dwpcloud.uk:8443/payment-dates/dates/2024-12-27?region=united-kingdom';
  console.log('TEST_URL', url);
  try {
    const res = await axios({
      method: 'get',
      url,
      headers: {
        accept: 'application/json',
        'Correlation-Id': '6647be62-1c12-45d7-9e42-d796955a6712',
        'instigating-system': 'BAM-UI',
      },
      validateStatus: false,
    });
    console.log('OK', res.status, res.data);
  } catch (e) {
    console.error('ERROR', e.code, e.message);
  }
}

go().then();
