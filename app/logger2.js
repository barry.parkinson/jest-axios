const os = require('os');
const winston = require('winston');

const winstonLogger = winston.createLogger({
  level: process.env.LOG_LEVEL || 'debug',
  format: winston.format.json(),
  transports: [new winston.transports.Console()],
});

function adjustMessage(message) {
  if (typeof message !== 'object' || Array.isArray(message) || message instanceof Date) {
    return { message };
  }
  if (message instanceof Buffer) {
    return { message: message.toString('utf8') };
  }
  return message;
}

function makeLogger(initialFields) {
  const fields = { ...initialFields };

  function log(level, message) {
    winstonLogger.log({
      ...fields,
      ...adjustMessage(message),
      level,
      ts: new Date().toISOString(),
    });
  }

  return {
    createLoggerForApp: (packageJson, values) => makeLogger({
      ...fields,
      app_name: packageJson.name,
      app_version: packageJson.version,
      ...values,
    }),
    createLogger: (values) => makeLogger({ ...fields, ...values }),
    error: (message) => log('error', message),
    warn: (message) => log('warn', message),
    info: (message) => log('info', message),
    debug: (message) => log('debug', message),
  };
}

module.exports = makeLogger({
  host_name: os.hostname(),
});
