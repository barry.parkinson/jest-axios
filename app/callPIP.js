const os = require('os');
const logAPIResponse = require('./logAPIResponse');
const axiosRequest = require('./axiosRequest');

function template() {
  return `<?xml version="1.0"?>
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
  <soapenv:Body>
    <ns1:{{operation}} xmlns:ns1="http://remote.interfaces.pipdataretrievalservices.pip.curam">
      <ns1:Document>
        <Request xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
          <Header>
            <UserID>EXT-IDNOUSER</UserID>
            <UserName>EXT-ID_NOUSER</UserName>
            <SystemID>752</SystemID>
            <SubSystemID>ExtIDIDV</SubSystemID>
            <HostName>{{HostName}}</HostName>
            <AccessLevel>1</AccessLevel>
            <SessionID>{{SessionID}}</SessionID>
            <DepartmentID>1</DepartmentID>
            <OfficeID>6012</OfficeID>
            <LocationAddress>{{LocationAddress}}</LocationAddress>
            <AuditFlag>true</AuditFlag>
            <VersionNumber>1.0</VersionNumber>
          </Header>
          <Input>
            <NINO>{{nino}}</NINO>
          </Input>
        </Request>
      </ns1:Document>
    </ns1:{{operation}}>
  </soapenv:Body>
</soapenv:Envelope>`;
}

const actions = {
  PIPGetPersonalNewDetails: 'getPersonalNewData',
  PIPGetPaymentDetails: 'getPaymentData',
  PIPGetEntitlement: 'getEntitlementData',
  PIPGetCaseStatus: 'PIPGetCaseStatus',
};

async function callPIP(req, api, nino) {
  const url = `${process.env.PIP_URL.replace(/\/+$/, '')}/${api}${process.env.INT_SUFFIX === '-dth2' ? process.env.INT_SUFFIX : ''}`;

  // one wonders if we really have to do this or is 'operation' just ignored?
  const operation = api === 'PIPGetPersonalNewDetails' ? 'PIPGetPersonalNewData' : api;

  const data = template()
    .replace(/{{operation}}/g, operation)
    .replace('{{nino}}', nino.toUpperCase())
    .replace('{{SessionID}}', req.sessionId)
    .replace('{{HostName}}', os.hostname())
    .replace('{{LocationAddress}}', '0.0.0.0'); // TODO fix with ip ?

  const response = await axiosRequest({
    method: 'POST',
    url,
    headers: {
      'Content-Type': 'text/xml;charset=UTF-8',
      SOAPAction: actions[api],
    },
    timeout: 3000,
    data,
  });
  logAPIResponse(req, api, response);
  return response;
}

module.exports = callPIP;
