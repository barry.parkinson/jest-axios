const crypto = require('crypto');

const keyTypes = {
  RS256: { type: 'rsa', modulusLength: 2048, opt: 'modulusLength' },
  RS384: { type: 'rsa', modulusLength: 2048, opt: 'modulusLength' },
  RS512: { type: 'rsa', modulusLength: 2048, opt: 'modulusLength' },
  PS256: { type: 'rsa', modulusLength: 2048, opt: 'modulusLength' },
  PS384: { type: 'rsa', modulusLength: 2048, opt: 'modulusLength' },
  PS512: { type: 'rsa', modulusLength: 2048, opt: 'modulusLength' },
  ES256: { type: 'ec', namedCurve: 'prime256v1', opt: 'namedCurve' },
  ES384: { type: 'ec', namedCurve: 'secp384r1', opt: 'namedCurve' },
  ES512: { type: 'ec', namedCurve: 'secp521r1', opt: 'namedCurve' },
  ES256K: { type: 'ec', namedCurve: 'secp256k1', opt: 'namedCurve' },
  EdDSA: { type: 'ed25519', opt: 'type' },
  EdDSA448: { type: 'ed448' },
};

function generateKeyPair(kind, optionalValue) {
  const keyType = keyTypes[kind];
  if (keyType) {
    const { opt, ...rest } = keyType;
    if (opt && optionalValue) {
      rest[opt] = opt === 'modulusLength' ? parseInt(optionalValue, 10) : optionalValue;
    }
    const { type, namedCurve, modulusLength } = rest;
    const keypair = crypto.generateKeyPairSync(type, {
      namedCurve,
      modulusLength,
      privateKeyEncoding: { format: 'pem', type: 'pkcs8' },
      publicKeyEncoding: { format: 'pem', type: 'spki' },
    });
    return keypair;
  }
  return undefined;
}

module.exports = generateKeyPair;
