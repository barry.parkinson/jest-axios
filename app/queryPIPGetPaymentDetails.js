const callPIP = require('./callPIP');

// pip_pay_day
// pip_claim_accepted_date (unused)
// pip_last_payment_amount
// pip_last_payment_date

async function queryPIPGetPaymentDetails(req, nino) {
  const res = await callPIP(req, 'PIPGetPaymentDetails', nino);
  console.log(res.data);
  return res;
}

module.exports = queryPIPGetPaymentDetails;
