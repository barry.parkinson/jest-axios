const callCIS = require('./callCIS');

// cis_partners_nino
// cis_partners_dob
// cis_childs_name
// cis_childs_dob

function log(tag, ...args) {
  process.stdout.write(`\x1b[36m${tag}\x1b[0m ${args.join(' ')}\n`);
}

const PARTNER_RELATIONSHIP_CODES = ['1', '2', '14'];
const CHILD_RELATIONSHIP_CODES = ['3'];

async function queryCISRelationships(req, ninoQuery) {
  const res = await callCIS(req, 'api-009', ninoQuery, true);
  if (!res.data.GetRelationshipDetailsResponse) {
    return;
  }
  const list = res.data.GetRelationshipDetailsResponse.personRoleRelationships;
  // add dates
  list.forEach((rel) => {
    rel.dob = new Date(rel.birthDate); // eslint-disable-line no-param-reassign
  });
  // oldest first
  list.sort((a, b) => {
    const ad = a.dob;
    const bd = b.dob;
    if (ad > bd) {
      return 1;
    }
    if (ad < bd) {
      return -1;
    }
    return 0;
  });
  // questions
  let childBirthDate;
  const childNames = [];
  const partnerNinos = [];
  const partnerBirthdays = [];
  // scan list
  list.forEach((rel) => {
    log('REL', JSON.stringify(rel));
    const {
      roleRelationshipTypeID,
      birthDate,
      nino,
      ninoSuffix,
      sourceFlag,
      personName,
    } = rel;
    if (sourceFlag !== '1') {
      return;
    }
    if (!personName || !personName.length) {
      return;
    }
    const {
      forenames,
      surname,
    } = personName[0];
    if (!forenames || !surname) {
      return;
    }
    // type
    if (PARTNER_RELATIONSHIP_CODES.includes(roleRelationshipTypeID)) {
      log('PARTNER', birthDate, nino, ninoSuffix);
      partnerNinos.push(`${nino}${ninoSuffix}`);
      partnerBirthdays.push(birthDate); // TODO format
    } else if (CHILD_RELATIONSHIP_CODES.includes(roleRelationshipTypeID)) {
      log('CHILD', birthDate, forenames, surname);
      if (!childBirthDate) {
        childBirthDate = birthDate;
      }
      if (birthDate === childBirthDate) {
        childNames.push(`${forenames} ${surname}`);
      }
    }
  });
  // done
  log('KBVS', childBirthDate, childNames, partnerNinos, partnerBirthdays);
}

module.exports = queryCISRelationships;
