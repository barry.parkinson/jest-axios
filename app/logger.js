const os = require('os');
const {
  createLogger,
  format,
  transports,
} = require('winston');

const winstonLogger = createLogger({
  level: process.env.LOG_LEVEL || 'debug',
  format: format.json(),
  transports: [new transports.Console()],
});

function init(pkg) {
  const defaults = {
    app_name: pkg.name,
    app_version: pkg.version,
    host_name: os.hostname(),
  };

  function logger(values) {
    const fields = {
      ...defaults,
      ...values,
    };

    function log(level, message) {
      winstonLogger.log({
        level,
        ...fields,
        ...message,
        ts: (new Date(Date.now())).toISOString(),
      });
    }

    return {
      error: (message) => log('error', message),
      warn: (message) => log('warn', message),
      info: (message) => log('info', message),
      debug: (message) => log('debug', message),
    };
  }

  return logger;
}

module.exports = init;
