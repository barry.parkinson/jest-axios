/* eslint-disable max-len */
const axios = require('axios');

// ignore coverage on validateStatus() as this is handled internally by axios, which is mocked in unit tests
/* istanbul ignore next */
const validateStatus = () => true; // do not throw exceptions if a response is received

/**
 * Make a HTTP request with axios - this method will never throw an error and will always return a response.
 * In the response, this method will also set 'requestTime' - the number of seconds which the thing took, ie a number: 0.035
 * @param config - standard axios config
 * @returns {Promise<*>} an axios response with status and data - status 666 indicates a timeout or connection error
 */

async function axiosRequest(config) {
  const start = Date.now();
  let response;
  try {
    const ax = {
      method: 'post', // default to post
      maxRedirects: 0,
      timeout: 20000,
      validateStatus,
      ...config, // can override any of the above
    };
    // if you wish to debug what is being sent, this is the place
    response = await axios(ax);
  } catch (error) {
    response = {
      status: error.code || 666, // this non-standard HTTP code indicates an error occurred, no response was received - typically a timeout or connection error
      data: {
        code: error.code,
        message: error.message,
      },
      headers: {},
    };
  }
  response.requestTime = (Date.now() - start) / 1000;
  return response;
}

module.exports = axiosRequest;
