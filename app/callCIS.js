const os = require('os');
// const ip = require('ip');
const logAPIResponse = require('./logAPIResponse');
const axiosRequest = require('./axiosRequest');

async function callCIS(req, api, nino, historyFlag) {
  const headers = {
    'Content-Type': 'application/json',
    userName: 'EXT-ID_NOUSER',
    userId: 'EXT-IDNOUSER',
    SystemID: '752',
    SubSystemID: 'ExtIDIDV',
    officeID: '6012',
    hostName: os.hostname(),
    accessLevel: '4',
    sessionId: req.sessionId,
    nino: nino.substring(0, 8)
      .toUpperCase(),
    DepartmentID: '1',
    // locationAddress: ip.address(),
    auditFlag: true,
    nationalSensitivityAccess: false,
    'X-CertificateOuid': 'extnl-id',
    'X-swimlaneId': '03',
    dwpServicekeys: ['1', '2', '85', '670', '675'], // TODO FULL LIST
    historyFlag,
  };

  const url = `${process.env.CIS_URL.replace(/\/+$/, '')}/${api}${process.env.INT_SUFFIX}`;
  const response = await axiosRequest({
    method: 'GET',
    url,
    headers,
    timeout: 3000,
  });
  logAPIResponse(req, api, response);
  return response;
}

module.exports = callCIS;
