const axiosRequest = require('./axiosRequest');
const logAPIResponse = require('./logAPIResponse');

async function callGuidService(req, api, dwpGuid) {
  const headers = {
    'dwp-guid': dwpGuid,
  };
  const url = `${process.env.GUID_SERVICE_URL.replace(/\/$/, '')}/guid-service`;
  const response = await axiosRequest({
    method: 'GET',
    url,
    headers,
    timeout: 3000,
  });
  logAPIResponse(req, api, response);
  return response;
}

module.exports = callGuidService;

// response: { "identifier": "MW000137A", "type": "NINO" }
