const xml2js = require('xml2js');
const {
  stripPrefix,
  normalize,
} = require('xml2js/lib/processors');
const callPIP = require('./callPIP');

function extract(tag, data) {
  const s = data.indexOf(`<${tag}>`);
  if (s < 0) {
    return undefined;
  }
  const e = data.indexOf(`</${tag}>`);
  return data.substring(s + tag.length + 2, e);
}

async function queryPIPGetPersonalNewDetails(req, nino) {
  const res = await callPIP(req, 'PIPGetPersonalNewDetails', nino);
  const acct = extract('BankAccountNumber', res.data);
  const sort = extract('BankSortCode', res.data);
  console.log(acct, sort);
  // parse xml
  console.log(res.data);
  const parser = new xml2js.Parser({
    explicitArray: true,
    tagNameProcessors: [stripPrefix, normalize],
  });
  const parsed = await parser.parseStringPromise(res.data);
  console.log(JSON.stringify(parsed, undefined, ' '));
  return res;
}

module.exports = queryPIPGetPersonalNewDetails;
