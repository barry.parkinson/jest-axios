module.exports = {
  extends: ['airbnb-base', 'eslint:recommended'],
  env: {
    node: true,
  },
  rules: {
    'no-plusplus': 'off',
  },
};
