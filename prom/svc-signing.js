// sigver_kms_sign{outcome="failed",status="key_not_found",context="SRA-DAM"} 0

const sign = {
  name: 'sigver_kms_sign',
  // count: 'sigver_kms_sign_count',
  // time: 'sigver_kms_sign_time_sum',
  success: ['200'],
  failed: ['error', 'key_not_found'],
  extra: {
    name: 'context',
    values: ['SRA-DAM', 'KBV-AAS'],
  },
  alert: '5%/10m', // alerts
  rate: 30, // emulation per minute?
  errorRate: 0,
  reqTime: 0.01,
  scenarios: [
    ['error', 'KBV-AAS'], // status & context
    ['key_not_found', 'BAD-CTX'],
  ],
};

// sigver_get_sign_ctx{outcome="success",status="200",context="KBV-AAS"} 0
// sigver_get_sign_ctx{outcome="failed",status="invalid_context",context="BAD-CTX"} 1

const ctx = {
  name: 'sigver_get_sign_ctx',
  // count: 'sigver_get_sign_ctx_count',
  // time: 'sigver_get_sign_ctx_time_sum',
  success: ['200'],
  failed: ['invalid_context', '502', '500', 'ECONNREFUSED'],
  extra: {
    name: 'context',
    values: ['SRA-DAM', 'KBV-AAS'],
  },
  alert: '5%/10m', // alerts
  rate: 60, // emulation per minute?
  errorRate: 0,
  reqTime: 0.01,
  scenarios: [['invalid_context', 'BAD-CTX']],
};

const verifyCtx = {
  ...ctx,
  name: 'sigver_get_verify_ctx',
  rate: 10,
};

const getPubKey = {
  ...sign,
  name: 'sigver_kms_get_public_key',
  rate: 2,
};

// sigver_kms_get_public_key
// sigver_get_verify_jwks
// sigver_get_secret

module.exports = {
  dashboard: 'KMS Sign Service',
  url: '/sign/metrics',
  extraLabel: ['context'],
  tags: [ctx, sign, getPubKey, verifyCtx],
};

// Cache
// Policy Context
// Decrypt
// Signing
// SRA-DAM

// 1. generate dashboard
// 2. generate alerts
// 3. emulate normal behaviour
// 4. emulate error situations
