const services = require('./serviceList');
const html = require('./staticPageContent');

function input(a, id, value) {
  a.push('<input type="text" class="small" id="');
  a.push(id);
  a.push('" value="');
  a.push(value);
  a.push('">');
}

function drawServices(a) {
  services.forEach((service) => {
    a.push('<p><b>');
    a.push(service.dashboard);
    a.push('</b> <a href="');
    a.push(service.url);
    a.push('">');
    a.push(service.url);
    a.push('</a>');
    a.push('</p>');

    a.push('<table>');
    a.push('<tr><th>Tag</th><th>cc</th><th>Rate</th><th>Errors</th><th>Time</th><th></th></tr>');
    service.tags.forEach((tag) => {
      const total = tag.totalCounter.hashMap[''].value; // undocumented
      a.push('<tr><td class="txt">');
      a.push(tag.name);
      a.push('</td><td class="txt">');
      a.push(total);
      a.push('</td><td>');
      input(a, `${tag.name}_rate`, tag.rate);
      a.push('</td><td>');
      input(a, `${tag.name}_error_rate`, tag.errorRate);
      a.push('</td><td>');
      input(a, `${tag.name}_req_time`, tag.reqTime);
      a.push('</td><td>');
      a.push(`<button onClick="setTag('${tag.name}')">Set</button>`);
      a.push('</td></tr>');
    });
    a.push('</table>');
  });
}

function render(req, res) {
  res.set('Cache-Control', ' no-store, no-cache, must-revalidate, proxy-revalidate');
  res.set('Expires', '0');
  const a = [html];
  drawServices(a);
  a.push('</body></html>');
  res.send(a.join(''));
}

module.exports = (app) => {
  app.get('/', render);
};
