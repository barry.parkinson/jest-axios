const html = `<html lang="en">
<head>
<title>Mock HCS Prometheus</title>
<style>
* { font-family: Arial, Helvetica, sans-serif; }
.small {
  width: 70px;
}
td.txt { padding-right: 12px; min-width: 40px; }
input, select, button {
  padding: 8px;
  font-size: 15px;
  height: 38px;
  box-sizing: border-box;
}
th { text-align: left; }
b { font-size: 125% }
p {
  background-color: #c2d3e1;
  padding: 12px; 
}
</style>
<script>
function setTag(t) {
  var rate = encodeURIComponent(document.getElementById(t + '_rate').value || 60);
  var errs = encodeURIComponent(document.getElementById(t + '_error_rate').value || 0);
  var time = encodeURIComponent(document.getElementById(t + '_req_time').value || 0.1);
  // window.alert(t+' '+rate+' '+errs+' '+time);
  fetch('/trigger/' + encodeURIComponent(t) + '/' + rate + '/' + errs + '/' + time)
    .then(response => console.log(response))
    .catch(e => console.error(e));
}
</script>
</head>
<body>
`;

module.exports = html;
