const services = require('./serviceList');

let interval = 0;
let second = 1;

function pick(arr) {
  if (arr) {
    const i = Math.floor(Math.random() * arr.length);
    return arr[i];
  }
  return undefined;
}

function calc(tag, rate) {
  const x = Math.floor((rate / 60) * (second - 1));
  const a = Math.floor((rate / 60) * second);
  return a - x;
}

function run() {
  services.forEach((service) => {
    service.tags.forEach((tag) => {
      // success
      let t = calc(tag, tag.rate);
      while (t > 0) {
        tag.increment(pick(tag.success), tag.reqTime, pick(tag.extra && tag.extra.values));
        t--;
      }
      // errors
      t = calc(tag, tag.errorRate);
      while (t > 0) {
        tag.increment(pick(tag.failed), tag.reqTime, pick(tag.extra && tag.extra.values));
        t--;
      }
    });
  });
  second++;
  if (second > 60) {
    second = 1;
  }
}

function start() {
  interval = setInterval(run, 1000);
}

function stop() {
  clearInterval(interval);
}

module.exports = {
  start,
  stop,
};
