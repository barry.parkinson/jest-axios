const signing = require('./svc-signing');
const cache = require('./svc-cache');

module.exports = [signing, cache];
