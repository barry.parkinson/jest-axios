const encrypt = {
  name: 'scs_encrypt',
  count: 'scs_encrypt_count',
  time: 'scs_encrypt_time_sum',
  success: ['200'],
  failed: ['500', '502', 'ECONNREFUSED'],
  alert: '5%/10m', // alerts
  rate: 40, // emulation per minute?
  errorRate: 0,
  reqTime: 0.025,
};

const decrypt = {
  name: 'scs_decrypt',
  count: 'scs_decrypt_count',
  time: 'scs_decrypt_time_sum',
  success: ['200'],
  failed: ['500', '502', 'ECONNREFUSED'],
  alert: '5%/10m', // alerts
  rate: 60, // emulation per minute?
  errorRate: 0,
  reqTime: 0.025,
};

// redis

const store = {
  name: 'scs_redis_store',
  success: ['ok'],
  failed: ['error'],
  alert: '5%/10m', // alerts
  rate: 40, // emulation per minute?
  errorRate: 0,
  reqTime: 0.01,
};

const retrieve = {
  ...store,
  success: ['ok', 'nf'],
  name: 'scs_redis_retrieve',
  rate: 60,
};

const del = {
  ...store,
  name: 'scs_redis_delete',
  rate: 10,
};

const exp = {
  ...store,
  name: 'scs_redis_expire',
  rate: 10,
};

module.exports = {
  dashboard: 'Secure Cache Service',
  url: '/cache/metrics',
  tags: [encrypt, decrypt, store, retrieve, del, exp],
};
