const client = require('prom-client');
const services = require('./serviceList');
const tagMap = require('./tagMap');
const log = require('./log');
const makeCounter = require('./makeCounter');
const makeCounterExtra = require('./makeCounterExtra');

function initPrometheus(app) {
  services.forEach((service) => {
    log('SERVICE', service.dashboard);

    const registry = new client.Registry();
    service.registry = registry; // eslint-disable-line no-param-reassign

    app.get(service.url, async (req, res) => {
      try {
        res.set('Cache-Control', ' no-store, no-cache, must-revalidate, proxy-revalidate');
        res.set('Expires', '0');
        res.set('Content-Type', registry.contentType);
        res.end(await registry.metrics());
      } catch (ex) {
        res.status(500)
          .end(ex);
      }
    });

    service.tags.forEach((tag) => {
      tagMap.set(tag.name, tag);
      // set defaults
      if (!tag.count) {
        tag.count = `${tag.name}_count`; // eslint-disable-line no-param-reassign
      }
      if (!tag.time) {
        tag.time = `${tag.name}_time_sum`; // eslint-disable-line no-param-reassign
      }
      // create the counter
      if (tag.extra) {
        makeCounterExtra(registry, tag);
      } else {
        makeCounter(registry, tag);
      }
    });
  });
}

module.exports = initPrometheus;
