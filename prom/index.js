const express = require('express');
const log = require('./log');
const prom = require('./initPrometheus');
const { start } = require('./runner');
const home = require('./home-page');
const trigger = require('./trigger');

const app = express();

app.set('x-powered-by', false);
app.set('etag', false);

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

prom(app);
trigger(app);
home(app);

start();

// start service
const port = process.env.PORT || 4039;

app.listen(port, () => {
  log(`PROM test app running on ${port}`);
});

module.exports = app;
