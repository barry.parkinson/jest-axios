const client = require('prom-client');

function makeCounterExtra(registry, tag) {
  const extraName = tag.extra.name;
  const total = new client.Counter({
    name: tag.count,
    help: 'help',
    registers: [registry],
  });
  total.inc(0);
  tag.totalCounter = total; // eslint-disable-line no-param-reassign
  const times = new client.Counter({
    name: tag.time,
    help: 'help',
    registers: [registry],
  });
  times.inc(0);
  // outcome and status
  const counter = new client.Counter({
    name: tag.name,
    help: 'help',
    labelNames: ['outcome', 'status', extraName],
    registers: [registry],
  });
  // set all to zero
  tag.extra.values.forEach((extraValue) => {
    tag.success.forEach((status) => {
      counter.inc({ outcome: 'success', status, [extraName]: extraValue }, 0);
    });
  });
  tag.extra.values.forEach((extraValue) => {
    tag.failed.forEach((status) => {
      counter.inc({ outcome: 'failed', status, [extraName]: extraValue }, 0);
    });
  });
  // inc
  tag.increment = (status, requestTime, exValue) => { // eslint-disable-line no-param-reassign
    total.inc();
    times.inc(requestTime);
    const outcome = tag.success.indexOf(status) >= 0 ? 'success' : 'failed';
    counter.inc({ outcome, status, [extraName]: exValue });
  };
}

module.exports = makeCounterExtra;
