const tagMap = require('./tagMap');
const log = require('./log');

function trigger(req, res) {
  const {
    tag,
    rate,
    errorRate,
    reqTime,
  } = req.params;
  const tagData = tagMap.get(tag);
  log('TRIG', tag, rate, errorRate, reqTime);
  if (tagData) {
    tagData.rate = parseInt(rate, 10);
    tagData.errorRate = parseInt(errorRate, 10);
    tagData.reqTime = parseFloat(reqTime);
  }
  res.send({});
}

module.exports = (app) => {
  app.get('/trigger/:tag/:rate/:errorRate/:reqTime', trigger);
};
