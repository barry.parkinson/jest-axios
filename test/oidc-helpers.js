/* eslint-disable max-len, camelcase */
const https = require('https');
const axios = require('axios');
const os = require('os');
const jwt = require('jsonwebtoken');

const OAUTH_PATH = '/am/oauth2/realms/root/realms/Citizens/realms/';

const OPTIONS = {
  maxRedirects: 0,
  httpsAgent: new https.Agent({ rejectUnauthorized: false }), // allow self-signed cert
  timeout: 20000,
  validateStatus: () => true,
};

function log(tag, ...args) {
  process.stdout.write(`\x1b[35m${tag}\x1b[0m ${args.join(' ')}\n`);
}

function makeAuthorizeUrl(config) {
  return `${config.host}${OAUTH_PATH}${config.realm}/authorize`
    + '?response_mode=query'
    + '&response_type=code'
    + '&nonce=e42cd44e66de3aac'
    + '&state=wDmf59iyp0VE_bz7RoO2gJqt'
    + `&redirect_uri=${encodeURIComponent(config.redirect)}`
    + `&client_id=${config.clientId}`
    + `&scope=${config.scope}`
    // + '&acr_sig=-OUtAq2F8zxG_1lnHzH5UOldlVGoTGvjgqDUfhiTu4c'
    // + `&acr=${acr}`
    + `&acr_values=${config.acr}`;
}

function extractParam(paramName, location) {
  if (location) {
    const i = location.indexOf('?');
    const params = location.substring(i + 1);
    const arr = params.split('&');
    const theParam = arr.find((p) => p.startsWith(`${paramName}=`));
    if (theParam) {
      const encoded = theParam.substring(paramName.length + 1);
      return decodeURIComponent(encoded);
    }
  }
  return undefined;
}

async function getAccessToken(code, config) {
  const data = 'grant_type=authorization_code'
    + `&client_id=${config.clientId}`
    + `&client_secret=${encodeURIComponent(config.secret)}`
    + `&code=${encodeURIComponent(code)}`
    + `&redirect_uri=${encodeURIComponent(config.redirect)}`;
  const response = await axios({
    method: 'POST',
    url: `${config.host}${OAUTH_PATH}${config.realm}/access_token`,
    data,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'user-agent': `${os.hostname()}/access_token`,
    },
    ...OPTIONS,
  });
  // log('ACCESS_TOKEN_URL', `${config.host}${OAUTH_PATH}${config.realm}/access_token`, data);
  log('ACCESS TOKEN:', response.status, JSON.stringify(response.data, null, ' '));
  return response.data;
}

async function introspect(accessToken, config) {
  const { secret } = config;
  const data = `client_id=${config.clientId}`
    + `&client_secret=${encodeURIComponent(secret)}`
    + `&token=${encodeURIComponent(accessToken)}`;
  const response = await axios({
    method: 'POST',
    url: `${config.host}${OAUTH_PATH}${config.realm}/introspect`,
    headers: {
      'user-agent': `${os.hostname()}/introspect`,
    },
    data,
    ...OPTIONS,
  });
  log('INTROSPECT:', response.status, JSON.stringify(response.data, null, ' '), data);
  return response.data;
}

async function userInfo(accessToken, config) {
  const info = await axios({
    method: 'GET',
    url: `${config.host}${OAUTH_PATH}${config.realm}/userinfo`,
    headers: {
      authorization: `Bearer ${encodeURIComponent(accessToken)}`,
      'user-agent': `${os.hostname()}/userinfo`,
    },
    ...OPTIONS,
  });
  log('USERINFO:', info.status, JSON.stringify(info.data, null, ' '));
  return info.data;
}

async function callAuthorize(tokenId, config) {
  if (!tokenId) {
    return undefined;
  }
  const gotoUrl = makeAuthorizeUrl(config);
  log('AUTHORIZE_URL', gotoUrl);
  const auth = await axios({
    method: 'post',
    url: gotoUrl,
    data: `decision=allow&csrf=${encodeURIComponent(tokenId)}`,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      cookie: `DTH_AaaS_Session=${encodeURIComponent(tokenId)}`,
      // 'session-id': sessionId,
      'user-agent': `${os.hostname()}/authorize`,
    },
    ...OPTIONS,
  });
  // get code from location
  const code = extractParam('code', auth.headers.location);
  log('CODE', code);
  if (!code || auth.status !== 302) {
    log('AUTH_PROBLEM', auth.status, auth.headers, auth.data);
    return undefined;
  }
  // token
  const tok = await getAccessToken(code, config);
  const decoded = jwt.decode(tok.id_token, { complete: true });
  log('JWT_DECODED:', JSON.stringify(decoded, null, ' '));
  // introspect
  await introspect(tok.access_token, config);
  await userInfo(tok.access_token, config);
  return tok;
}

async function endSession(token, config) { // note token is the JWT id_token
  const url = [
    `${config.host}${OAUTH_PATH}${config.realm}/connect/endSession`,
    '?id_token_hint=',
    token,
    '&client_id=',
    config.clientId,
    // '&post_logout_redirect_uri',
    // encodeURIComponent('http://localhost:8080/xxx/logout'),
  ].join('');
  log('END_URL', url);
  const res = await axios({
    url,
    method: 'GET',
    ...OPTIONS,
  });
  // this appears to just return 204
  // if the token is not valid returns 400 with:
  // {"error_description":"Invalid id_token_hint: unable to reconstruct JWT","error":"bad_request"}
  log('END_SESSION', res.status, res.headers, '\nDATA', JSON.stringify(res.data));
}

async function sessionLogout(sso_token, config) {
  const url = `${config.host}/am/json/realms/root/realms/Citizens/realms/${config.realm}/sessions/?_action=logout`;
  log('LOGOUT_URL', url);
  const res = await axios({
    url,
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Accept-API-Version': 'protocol=1.0,resource=2.1',
      cookie: `DTH_AaaS_Session=${encodeURIComponent(sso_token)}`,
    },
    ...OPTIONS,
  });
  // status should be 200
  // data should be {"result":"Successfully logged out"}
  log('LOGOUT', res.status, res.headers, JSON.stringify(res.data));
}

async function callAMTree(headers, config, tree) {
  const ax = {
    method: 'post',
    url: `${config.host}/am/json/realms/root/realms/Citizens/realms/${config.realm}/authenticate?authIndexType=service&authIndexValue=${tree}`,
    headers: {
      'Accept-API-Version': 'protocol=1.0,resource=2.1',
      // 'Content-Type': 'application/x-www-form-urlencoded',
      ...headers,
    },
    data: '',
    ...OPTIONS,
  };
  // log('AXIOS', JSON.stringify(ax));
  return axios(ax);
}

async function startAMRecording(config) {
  const ax = {
    method: 'post',
    url: `${config.host}/am/json/realms/root/realms/root/authenticate?authIndexType=service&authIndexValue=adminconsoleservice`,
    headers: {
      'Accept-API-Version': 'protocol=1.0,resource=2.1',
      'X-OpenAM-Username': 'amadmin',
      'X-OpenAM-Password': 'admin@321',
    },
    data: '',
    ...OPTIONS,
  };
  // log('AXIOS', JSON.stringify(ax));
  const res = await axios(ax);
  log('AUTHENTICATE', res.status, JSON.stringify(res.data), '\n', JSON.stringify(res.headers, null, ' '));

  // https://backstage.forgerock.com/docs/am/7.2/maintenance-guide/record-troubleshooting.html
  const ax2 = {
    method: 'post',
    url: `${config.host}/am/json/records?_action=start`,
    headers: {
      'Accept-API-Version': 'resource=1.0',
      'Content-Type': 'application/json',
      iPlanetDirectoryPro: res.data.tokenId,
      cookie: `DTH_AaaS_Session=${res.data.tokenId}`,
    },
    data: {
      issueID: 88273,
      referenceID: 'upgradeFails',
      description: 'Troubleshooting 88273',
      zipEnable: true,
      configExport: {
        enable: true,
        password: '5x2RR70',
        sharePassword: false,
      },
      debugLogs: {
        debugLevel: 'MESSAGE',
        autoStop: {
          time: {
            timeUnit: 'SECONDS',
            value: 15,
          },
        },
      },
      threadDump: {
        enable: true,
      },
    },
  };
  log('SENDING', JSON.stringify(ax2));
  const res2 = await axios({ ...ax2, ...OPTIONS });
  log('RECORDING', res2.status, JSON.stringify(res2.data));
  return res2;
}

function formData(obj) {
  return Object.keys(obj)
    .map((key) => `${key}=${encodeURIComponent(obj[key])}`)
    .join('&');
}

module.exports = {
  callAuthorize,
  callAMTree,
  startAMRecording,
  endSession,
  sessionLogout,
  introspect,
  userInfo,
  formData,
  extractParam,
  OPTIONS,
};
