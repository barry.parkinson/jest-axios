/* eslint-disable max-len */
const crypto = require('crypto');
const {
  KMSClient,
  EncryptCommand,
  DecryptCommand,
  GetPublicKeyCommand,
} = require('@aws-sdk/client-kms');

const log = (tag, ...args) => console.log(`\x1b[36m${tag}\x1b[0m`, ...args); // eslint-disable-line no-console

// https://docs.aws.amazon.com/AWSJavaScriptSDK/v3/latest/client/kms/command/EncryptCommand/
// https://docs.aws.amazon.com/AWSJavaScriptSDK/v3/latest/client/kms/command/DecryptCommand/

async function getPublicKey(client, KeyId) {
  const command = new GetPublicKeyCommand({ KeyId });
  const response = await client.send(command);
  const buff = Buffer.from(response.PublicKey);
  const str = buff.toString('base64');
  // format it
  const a = ['-----BEGIN PUBLIC KEY-----'];
  for (let n = 0; n < str.length; n += 64) {
    a.push(str.substring(n, n + 64));
  }
  a.push('-----END PUBLIC KEY-----');
  return a.join('\n');
}

describe('KMS encryption:', () => {
  const client = new KMSClient({
    region: 'eu-west-2',
  });

  test('encrypt and decrypt', async () => {
    const KeyId = '27028cda-8eb5-497c-a763-290cef35ff60';
    // const KeyId = 'arn:aws:kms:eu-west-2:943009210227:key/1a8494b1-55f9-4d1f-beed-3c0b4cffaa7f'; // HCS key
    const original = 'This is the string. Code 5847 £50 ëvǒ';

    const response = await client.send(new EncryptCommand({
      KeyId,
      Plaintext: Buffer.from(original),
      // EncryptionContext: { "<keys>": "STRING_VALUE", },
      GrantTokens: [],
      EncryptionAlgorithm: 'RSAES_OAEP_SHA_256',
      // DryRun: true || false,
    }));
    log('ENCRYPTED', response);

    // decrypt
    const decrypted = await client.send(new DecryptCommand({
      KeyId,
      CiphertextBlob: response.CiphertextBlob,
      GrantTokens: [],
      EncryptionAlgorithm: 'RSAES_OAEP_SHA_256',
    }));

    // assert
    log('DECRYPTED', decrypted);
    const check = Buffer.from(decrypted.Plaintext)
      .toString('utf8');
    log('CHECK', check);
    expect(check)
      .toEqual(original);
  });

  test('encrypt using public key and decrypt using KMS', async () => {
    const original = 'This is the string. Code 5847 £50 ëvǒ ===';

    const KeyId = '27028cda-8eb5-497c-a763-290cef35ff60';
    const publicKey = await getPublicKey(client, KeyId);

    const encryptedBuffer = crypto.publicEncrypt({
      key: publicKey,
      oaepHash: 'sha256',
    }, Buffer.from(original));
    log('ENCRYPT WITH PUBLIC KEY', encryptedBuffer.toString('base64'));

    // decrypt
    const decrypted = await client.send(new DecryptCommand({
      KeyId,
      CiphertextBlob: encryptedBuffer,
      GrantTokens: [],
      EncryptionAlgorithm: 'RSAES_OAEP_SHA_256', // must match oaepHash above
    }));

    // assert
    log('DECRYPTED WITH KMS', decrypted);
    const check = Buffer.from(decrypted.Plaintext)
      .toString('utf8');
    log('CHECK', check);
    expect(check)
      .toEqual(original);
  });
});
