const axios = require('axios');

function log(tag, ...args) {
  process.stdout.write(`\x1b[36m${tag}\x1b[0m ${args.join(' ')}\n`);
}

async function waitFor(t) {
  return new Promise((resolve) => {
    setTimeout(() => resolve(), t);
  });
}

async function callPost(props) {
  return axios({
    method: 'post',
    validateStatus: () => true,
    ...props,
  });
}

describe('monorepo', () => {
  jest.setTimeout(20000);

  test('default page works', async () => {
    const res = await axios({
      url: 'http://localhost:8080',
    });
    log('RESPONSE', JSON.stringify(res.data));
    expect(res.status)
      .toEqual(200);
  });

  test('ESA works', async () => {
    const nino = 'AE767802B';
    const res = await callPost({
      url: 'http://localhost:8080/esa-kbv/request',
      data: { nino },
    });
    log('ESA REQUEST', res.status, JSON.stringify(res.data));
    expect(res.status)
      .toEqual(200);
    // have to wait
    await waitFor(12000);
    const res2 = await callPost({
      url: 'http://localhost:8080/esa-kbv/retrieve',
      data: { nino },
    });
    log('ESA RETRIEVE', res2.status, JSON.stringify(res2.data));
    expect(res.status)
      .toEqual(200);
  });

  test('Invalid Route, Headers and Data', async () => {
    const json = JSON.stringify({
      'password-reset': {
        timestamp: 1677234427175,
        numberOfLockouts: 1,
      },
      'account-recovery': {
        timestamp: 1677234427121,
        numberOfLockouts: 2,
      },
      authenticate: {},
    });
    const res = await callPost({
      url: 'http://localhost:8080/no-such-api',
      headers: {
        'x-dwp-extra': 'bad:characters=who"knows"?+&!',
        'x-another': '',
        'x-temp-locked-out': json,
        'x-undefined': undefined,
        'User-Agent': '£45.50 please',
      },
      data: 'some-text=here\r\nit is',
    });
    log('INVALID', res.status, JSON.stringify(res.data));
    expect(res.status)
      .toEqual(404);
  });
});
