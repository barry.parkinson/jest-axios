const escape = require('../tidv/escape');

function log(tag, ...args) {
  process.stdout.write(`\x1b[36m${tag}\x1b[0m ${args.join(' ')}\n`);
}

describe('escape', () => {
  test('json', () => {
    const text = '{"fieldId":"kbv_unavailable","verifiedValue":""}';
    const out = escape(text);
    // eslint-disable-next-line no-console
    console.log(out);
  });

  test('others', () => {
    const text = 'This&That<script>';
    const out = escape(text);
    console.log(out); // eslint-disable-line no-console
  });

  test('not strings', () => {
    console.log('boolean', escape(true)); // eslint-disable-line no-console
    console.log('number', escape(42.28)); // eslint-disable-line no-console
    console.log('date', escape(new Date())); // eslint-disable-line no-console
    console.log('zero', escape(0)); // eslint-disable-line no-console
    console.log('null', escape(null)); // eslint-disable-line no-console
    console.log('undefined', escape(undefined)); // eslint-disable-line no-console
    console.log('empty', escape('')); // eslint-disable-line no-console
    console.log('{foo}', escape({ foo: 'bar' })); // eslint-disable-line no-console
    console.log('[]', escape([])); // eslint-disable-line no-console
    console.log('[]', escape(['abc<b>123', 'X&Y'])); // eslint-disable-line no-console
  });

  test('loop', () => {
    const rate = 20;
    let acc = 0;
    for (let second = 1; second < 61; second++) {
      const a = Math.floor((rate / 60) * second);
      log(second, a - acc, a);
      acc = a;
    }
  });
});
