const axios = require('axios');
const jwt = require('jsonwebtoken');
const {
  OPTIONS,
  formData,
  extractParam,
} = require('./oidc-helpers');

const CONFIGS = {
  WEB: {
    tokenUrl: 'http://localhost:28080/realms/WEB/protocol/openid-connect/token',
    introspectUrl: 'http://localhost:28080/realms/WEB/protocol/openid-connect/token/introspect',
    userinfoUrl: 'http://localhost:28080/realms/WEB/protocol/openid-connect/userinfo',
    clientId: 'PIPAPPLY-WEB',
    scope: 'openid phone guid',
    secret: '0lHFIlQt6tx2dE0XqtcvnJjbadmkKTEt',
    adminSecret: 'c0BWhYFZSvwRw6dahu43R2mDIUSIADdA',
    usersUrl: 'http://localhost:28080/admin/realms/WEB/users',
    passwordRequired: true,
    email: 'sergey@example.com',
    authUrl: 'http://localhost:28080/realms/WEB/protocol/openid-connect/auth',
  },
  TIDV: {
    tokenUrl: 'http://localhost:28080/realms/TIDV/protocol/openid-connect/token',
    introspectUrl: 'http://localhost:28080/realms/TIDV/protocol/openid-connect/token/introspect',
    userinfoUrl: 'http://localhost:28080/realms/TIDV/protocol/openid-connect/userinfo',
    clientId: 'CxP-PIP-TIDV',
    scope: 'openid guid get:idv-failure get:benefit create:voiceprint',
    secret: 'uft2WTFiixw93On7TQm2c1r1UQOeSizB',
    adminSecret: 'spQMDK20o381ulaozTrar5pz0nrkllVR',
    usersUrl: 'http://localhost:28080/admin/realms/TIDV/users',
    passwordRequired: false,
    authUrl: 'http://localhost:28080/realms/TIDV/protocol/openid-connect/auth',
  },
};

const config = CONFIGS.WEB;

function log(tag, ...args) {
  process.stdout.write(`\x1b[36m${tag}\x1b[0m ${args.join(' ')}\n`);
}

function logIf(condition, tag, ...args) {
  if (condition) {
    log(tag, args);
  }
}

describe('keycloak', () => {
  beforeEach(() => {
    process.stdout.write(`\x1b[34m---- TEST ---- ${config.clientId} - ${expect.getState().currentTestName} ----------\n`);
  });

  let createdUserId = '';

  async function getAdminToken() {
    return axios({
      url: config.tokenUrl,
      method: 'post',
      ...OPTIONS,
      headers: {
        'content-type': 'application/x-www-form-urlencoded',
      },
      data: formData({
        client_id: 'admin-cli',
        client_secret: config.adminSecret,
        grant_type: 'client_credentials',
      }),
    });
  }

  async function introspect(token, headers) {
    const intro = await axios({
      url: config.introspectUrl,
      method: 'post',
      ...OPTIONS,
      headers: {
        'content-type': 'application/x-www-form-urlencoded',
        ...headers,
      },
      data: formData({
        client_id: config.clientId,
        client_secret: config.secret,
        token,
      }),
    });
    log('INTROSPECT', intro.status, JSON.stringify(intro.data, undefined, ' '));
    return intro.data;
  }

  async function userinfo(token) {
    const info = await axios({
      url: config.userinfoUrl,
      method: 'get',
      ...OPTIONS,
      headers: {
        // 'content-type': 'application/x-www-form-urlencoded',
        Authorization: `Bearer ${token}`,
      },
      // data: '',
    });
    log('USERINFO', info.status, JSON.stringify(info.data, undefined, ' '));
    logIf(info.status !== 200, 'USERINFO_ERROR', info.headers);
    // this can return a 403 forbidden error with a www-authenticate header
    // eslint-disable-next-line max-len
    // www-authenticate: Bearer realm="WEB", error="insufficient_scope", error_description="Missing openid scope"
    return info.data;
  }

  test('create a user', async () => {
    // https://keycloak.discourse.group/t/register-user-using-rest-api-in-keycloak/14390/8
    const res = await getAdminToken();
    const { data } = res;
    log('ADMIN_TOKENS', res.status, JSON.stringify(data, undefined, ' '));
    const accessToken = jwt.decode(data.access_token, { complete: true });
    log('ADMIN_ACCESS_TOKEN:', JSON.stringify(accessToken, null, ' '));

    // CREATE A USER
    const create = await axios({
      url: config.usersUrl,
      method: 'post',
      ...OPTIONS,
      headers: {
        authorization: `Bearer ${data.access_token}`,
      },
      data: {
        username: 'sergey',
        // firstName: 'Sergey',
        // lastName: 'Kargopolov',
        email: config.email,
        emailVerified: config.email ? true : undefined,
        enabled: true,
        groups: [],
        credentials: config.passwordRequired ? [{
          type: 'password',
          value: 'abc123',
          temporary: false,
        }] : undefined,
        attributes: {
          guid: '9f9f46d8b2115fc66510020863a182abf3c03b0501a1b3bde5d88f089a4d2857',
          phoneNumber: '079840333333',
        },
      },
    });
    log('CREATE', create.status, JSON.stringify(create.data, undefined, ' '));
    log('HEADERS', create.headers);
    // status 409 - { "errorMessage": "User exists with same username" }
    // status 409 - { "errorMessage": "User exists with same email" }
    // the user id is in the location header
    // location: http://localhost:28080/admin/realms/WEB/users/eb3bf923-c81f-4c42-b92f-4f09aca8a2db

    const gets = await axios({
      url: create.headers.location,
      method: 'get',
      ...OPTIONS,
      headers: {
        authorization: `Bearer ${res.data.access_token}`,
      },
    });
    log('GET_NEW_USER', gets.status, JSON.stringify(gets.data, undefined, ' '));
    createdUserId = gets.data.id;
  });

  test('update a user', async () => {
    const uid = createdUserId || '018a7cee-55a6-499b-8572-d0d2ee1d7425';
    // const uid = 'sue'; // 404
    const res = await getAdminToken();
    const update = await axios({
      url: `${config.usersUrl}/${uid}`,
      method: 'put',
      ...OPTIONS,
      headers: {
        authorization: `Bearer ${res.data.access_token}`,
      },
      data: {
        email: config.email,
        credentials: config.passwordRequired ? [{
          type: 'password',
          value: 'xpassword',
          temporary: false,
        }] : undefined,
        attributes: {
          guid: '777d219a8ac9abda7b3609bb7d132748f13c2ccfac924cea9662962871ae08a0',
          phoneNumber: '07940111111',
        },
      },
    });
    log('UPDATE_USER', update.status, JSON.stringify(update.data, undefined, ' '));
    // 204 is ok
  });

  test('get token by user password', async () => {
    const res = await axios({
      url: config.tokenUrl,
      method: 'post',
      ...OPTIONS,
      headers: {
        'content-type': 'application/x-www-form-urlencoded',
      },
      data: formData({
        client_id: config.clientId,
        client_secret: config.secret,
        grant_type: 'password',
        username: 'sergey',
        password: config.passwordRequired ? 'xpassword' : undefined,
        scope: config.scope,
        // acr_values: 'Auth-Level-Medium',
        claims: JSON.stringify({
          id_token: {
            acr: {
              values: ['Auth-Level-Medium'],
              essential: true,
            },
          },
        }),
      }),
    });
    const { data } = res;
    log('TOKEN_RESPONSE', res.status, JSON.stringify(data, undefined, ' '));

    const idToken = jwt.decode(data.id_token, { complete: true });
    log('ID_TOKEN:', JSON.stringify(idToken, null, ' '));

    const accessToken = jwt.decode(data.access_token, { complete: true });
    log('ACCESS_TOKEN:', JSON.stringify(accessToken, null, ' '));

    const refreshToken = jwt.decode(data.refresh_token, { complete: true });
    log('REFRESH_TOKEN:', JSON.stringify(refreshToken, null, ' '));

    // INTROSPECT
    await introspect(data.access_token);

    // USERINFO
    await userinfo(data.access_token);

    // REFRESH THE TOKEN
    const refresh = await axios({
      url: config.tokenUrl,
      method: 'post',
      ...OPTIONS,
      headers: {
        'content-type': 'application/x-www-form-urlencoded',
      },
      data: formData({
        client_id: config.clientId,
        client_secret: config.secret,
        grant_type: 'refresh_token',
        refresh_token: data.refresh_token,
      }),
    });

    log('REFRESH', refresh.status, JSON.stringify(refresh.data, undefined, ' '));
  });

  function parseSetCookies(c) {
    return c.map((cookie) => cookie.split(';')[0])
      .join('; ');
  }

  test('auth get login page and sign in with password', async () => {
    // note Authorization Code flow requires a password
    if (!config.authUrl) {
      return;
    }
    const params = formData({
      response_type: 'code',
      client_id: config.clientId,
      redirect_uri: 'http://localhost:3002/auth/callback',
      scope: config.scope,
      // nonce: whXh3eQjhx%2F77EH1alQO&
      state: 'BTKESqTlm0AnoNqheReX7UTW',
    });
    const formResponse = await axios({
      url: `${config.authUrl}?${params}`,
      method: 'get',
      ...OPTIONS,
    });

    const { data } = formResponse;
    log('LOGIN', formResponse.status, formResponse.headers, formResponse.data);

    // extract the form action url
    const i = data.indexOf('action="');
    if (i < 0) {
      return;
    }
    const start = data.substring(i + 8);
    const e = start.indexOf('"');
    const base = start.substring(0, e);
    const action = base.replace(/&amp;/g, '&');
    log('ACTION', action);

    const cookie = parseSetCookies(formResponse.headers['set-cookie']);
    log('COOKIE', cookie);

    const res = await axios({
      url: action,
      method: 'post',
      ...OPTIONS,
      headers: {
        'content-type': 'application/x-www-form-urlencoded',
        cookie,
      },
      data: formData({
        username: 'sergey',
        password: 'xpassword',
        // password: config.passwordRequired ? 'xpassword' : '', // DOES NOT WORK
      }),
    });
    // log('FINAL', res.status, res.headers, res.data);
    log('LOGIN_REDIRECT', res.headers.location);

    const code = extractParam('code', res.headers.location);
    log('CODE', code);

    const token = await axios({
      url: config.tokenUrl,
      method: 'post',
      ...OPTIONS,
      headers: {
        'content-type': 'application/x-www-form-urlencoded',
      },
      data: formData({
        code,
        client_id: config.clientId,
        client_secret: config.secret,
        redirect_uri: 'http://localhost:3002/auth/callback',
        grant_type: 'authorization_code',
        scope: config.scope,
        claims: JSON.stringify({
          id_token: {
            acr: {
              values: ['Auth-Level-Medium'],
              essential: true,
            },
          },
        }),
      }),
    });
    log('TOKEN_RESPONSE', token.status, JSON.stringify(token.data, undefined, ' '));
    const idToken = jwt.decode(token.data.id_token, { complete: true });
    log('ID_TOKEN:', JSON.stringify(idToken, null, ' '));
    await introspect(token.data.access_token);
  });

  test('get account client_credentials', async () => {
    const res = await axios({
      url: config.tokenUrl,
      method: 'post',
      ...OPTIONS,
      headers: {
        'content-type': 'application/x-www-form-urlencoded',
      },
      data: formData({
        client_id: config.clientId,
        client_secret: config.secret,
        grant_type: 'client_credentials',
        scope: 'openid offline_access',
      }),
    });
    logIf(res.status !== 200, 'CC_RESPONSE', res.status, JSON.stringify(res.data, undefined, ' '));
    const accessToken = jwt.decode(res.data.access_token, { complete: true });
    log('CC_ACCESS_TOKEN:', JSON.stringify(accessToken, null, ' '));
    await introspect(res.data.access_token, {
      // accept: 'application/token-introspection+jwt',
      // accept: 'application/jwt', // 406 "error": "RESTEASY003635: No match for accept header"
    });
    await userinfo(res.data.access_token);
  });
});
