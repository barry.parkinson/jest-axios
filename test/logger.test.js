const initLogger = require('../app/logger');
const Logger2 = require('../app/logger2');

describe('logger', () => {
  test('logging', () => {
    const makeLogger = initLogger({ name: 'my-app', version: '1.0.1' });
    const logger = makeLogger({ sessionId: '420000242' });
    logger.debug({ message: 'hello' });
    logger.info({ detail: '42' });
  });

  test('logger 2', () => {
    const baseLogger = Logger2.createLoggerForApp({ name: 'my-app2', version: '1.0.2' });
    const logger = baseLogger.createLogger({ sessionId: '420000242', xxx: 200 });
    logger.debug({ message: 'hello' });
    logger.info({ detail: '42' });
    // another
    const other = Logger2.createLogger({ app: 'foo' });
    other.warn({ message: { thing: 66, text: 'Hoho' } });
    // odd cases
    Logger2.warn('FOO-BAR');
    Logger2.error(new Date());
    Logger2.debug(['FOO-BAR', 42, {}, new Date()]);
    Logger2.debug();
    Logger2.debug(true);
    Logger2.debug(784738.454);
    // buffer
    const b = Buffer.from('abcdefg');
    Logger2.info(b);
    // override
    const lz = Logger2.createLogger({ host_name: undefined, corr_id: 'e48758474', level: 'info' });
    lz.debug('No host name');
    lz.info({ host_name: 'host666', corr_id: undefined });
  });
});
