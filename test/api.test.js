/* eslint-disable max-len */
const xml2js = require('xml2js');
const {
  stripPrefix,
  normalize,
} = require('xml2js/lib/processors');
const callPIP = require('../app/callPIP');
const callCIS = require('../app/callCIS');
const callGuidService = require('../app/callGuidService');
const callESA = require('../app/callESA');
const callMultiple = require('../app/callMultiple');
const queryCISRelationships = require('../app/queryCISRelationships');
const queryPIPGetPersonalNewDetails = require('../app/queryPIPGetPersonalNewDetails');
const queryPIPGetPaymentDetails = require('../app/queryPIPGetPaymentDetails');
const queryESA = require('../app/queryESA');

function log(tag, ...args) {
  process.stdout.write(`\x1b[36m${tag}\x1b[0m ${args.join(' ')}\n`);
}

describe('apis', () => {
  jest.setTimeout(30000);
  process.env.GUID_SERVICE_URL = 'http://localhost:8080';
  process.env.PIP_URL = 'http://localhost:8080/';
  process.env.CIS_URL = 'http://localhost:8080/cis/get/';
  process.env.INT_SUFFIX = '-shared';
  process.env.IAG_URL = 'http://localhost:8080/esa-kbv';
  process.env.IAG_API_KEY = 'iagApiKey';

  const req = {
    logger: {
      info: (d) => log('INFO', new Date().toISOString(), JSON.stringify(d)),
      error: (d) => log('ERROR', new Date().toISOString(), JSON.stringify(d, undefined, ' ')),
    },
  };

  test('pip', async () => {
    // const response = await callPIP(req, 'PIPGetPersonalNewDetails', 'PT568951D');
    // const response = await callPIP(req, 'PIPGetPaymentDetails', 'PA333485B'); // multi
    // const response = await callPIP(req, 'PIPGetEntitlement', 'PA333485B');
    const response = await callPIP(req, 'PIPGetCaseStatus', 'PA333485B');
    log('HEADERS', response.headers);
    log('DATA', response.data);
    const parser = new xml2js.Parser({
      explicitArray: false,
      tagNameProcessors: [stripPrefix, normalize],
    });
    const result = await parser.parseStringPromise(response.data);
    log('PARSED', JSON.stringify(result, undefined, ' '));
  });

  test('pip not found', async () => {
    const response = await callPIP(req, 'PIPGetPersonalNewDetails', 'QQ000000X');
    log('HEADERS', response.headers);
  });

  test('PIP error', async () => {
    const text = `<?xml version="1.0" encoding="UTF-8"?>
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
  <soapenv:Body>
    <ns:getPaymentDataResponse xmlns:ns="http://remote.interfaces.pipdataretrievalservices.pip.curam">
      <ns1:Document xmlns:ns1="http://remote.interfaces.pipdataretrievalservices.pip.curam">
        <GetPaymentDetailsResponse>
          <ResponseHeader>
            <EventMessages>
              <EventMessage>
                <MessageType>Error</MessageType>
                <MessageCode>0001</MessageCode>
                <MessageLocale>en_GB</MessageLocale>
                <MessageText>There is no record for this NINO / CRN held in PIPCS</MessageText>
              </EventMessage>
            </EventMessages>
          </ResponseHeader>
          <PaymentDetails/>
        </GetPaymentDetailsResponse>
      </ns1:Document>
    </ns:getPaymentDataResponse>  
  </soapenv:Body>
</soapenv:Envelope>`;
    const parser = new xml2js.Parser({
      explicitArray: false,
      tagNameProcessors: [stripPrefix, normalize],
    });
    const result = await parser.parseStringPromise(text);
    log('PARSED', JSON.stringify(result, undefined, ' '));
  });

  test('PIP parse error', async () => {
    // const text = '<{"error":544}';
    const text = '<stuff><thing>some text</thing></thing>';
    const parser = new xml2js.Parser({});
    try {
      const result = await parser.parseStringPromise(text);
      log('PARSED', JSON.stringify(result, undefined, ' '));
    } catch (e) {
      log('ERROR', JSON.stringify(e.message));
    }
  });

  test('XML parsing', async () => {
    // const text = '<foo:thing><a>100</a><b>9282</b></foo:thing>'; // a is NOT an array
    // const text = '<foo:thing><a>100</a><a>1002</a><b>9282</b></foo:thing>'; // a is an array
    const text = '<foo:thing><A><q>100</q></A><a>1002</a><b>9282</b></foo:thing>'; // a is an array
    const parser = new xml2js.Parser({
      explicitArray: false, // if true forces a & b (everything named) to be arrays
      tagNameProcessors: [stripPrefix, normalize],
    });
    const result = await parser.parseStringPromise(text);
    log('PARSED', JSON.stringify(result, undefined, ' '));
  });

  test('CIS', async () => {
    // const api = 'api-014'; // getFullAwardHistoryResponse
    // const api = 'api-008'; // GetContactDetailsResponse
    const api = 'api-009'; // GetRelationshipDetailsResponse
    // const api = 'api-006'; // GetNameDetailsResponse
    // const api = 'api-007'; // getAddressDetailsResponse
    // const api = 'api-038'; // risk state
    const res = await callCIS(req, api, 'PA333485B', true);
    log('CIS', JSON.stringify(res.data, undefined, '  '));
  });

  test('CIS RELS', async () => {
    // await queryCISRelationships(req, 'PA333485B'); // partner & child
    // await queryCISRelationships(req, 'PT568951D'); // partner & 2 child
    // await queryCISRelationships(req, 'AE767824C'); // 1 child (another is invalid)
    // await queryCISRelationships(req, 'AE767823C'); // 2 children
    // await queryCISRelationships(req, 'AE767804B'); // 1 child only
    // await queryCISRelationships(req, 'AE767805B'); // bad source flag
    await queryCISRelationships(req, 'GN129520A');
  });

  test('PIP BANK', async () => {
    // await queryPIPGetPersonalNewDetails(req, 'PA333485B');
    // await queryPIPGetPersonalNewDetails(req, 'AE328171B');
    await queryPIPGetPaymentDetails(req, 'PA333485B');
  });

  test('ESA', async () => {
    async function waitFor(t) {
      return new Promise((resolve) => {
        setTimeout(() => resolve(), t);
      });
    }
    // const nino = 'AE767806A'; // has bank acc
    const nino = 'AE790802B';
    // const nino = 'MW200001A'; // BSoc
    await callESA(req, 'request', nino, 'phone');
    await waitFor(1000);
    await queryESA(req, nino);
  });

  test('CIS MULTI 2', async () => {
    req.sessionId = 'KBV-TEST';
    // const nino = 'AE767822C'; // no PIP
    // const nino = 'AE767810C'; // PIP & ESA
    const nino = 'AE767808A'; // CIS only
    await Promise.allSettled([
      callMultiple(req, nino, false),
      callCIS(req, 'api-008', nino, true),
      callCIS(req, 'api-009', nino, true),
    ]);
  });

  test('CIS MULTI', async () => {
    req.sessionId = 'KBV-TEST';
    const start = Date.now();
    const nino = 'AE767807B';
    const results = await Promise.allSettled([
      callCIS(req, 'api-014', nino, true),
      callCIS(req, 'api-008', nino, true),
      callCIS(req, 'api-009', nino, true),
    ]);
    results.forEach((result) => {
      const res = result.value;
      log('CIS MULTI', result.status, res.status, JSON.stringify(res.data));
    });
    const awards = results[0].value.data;
    const ids = awards.getFullAwardHistoryResponse.personRoleServiceAssmtMap.map((award) => {
      log('AWARD', award.dwpServiceKeyID); // PIP 675, ESA 85
      return award.dwpServiceKeyID;
    });
    if (ids.includes('675')) {
      log('PIP');
      const pips = await Promise.allSettled([
        callPIP(req, 'PIPGetPersonalNewDetails', nino),
        callPIP(req, 'PIPGetPaymentDetails', nino),
        callPIP(req, 'PIPGetEntitlement', nino),
      ]);
      pips.forEach((result) => {
        const res = result.value;
        log('PIP MULTI', result.status, res.status); // res.data is xml
      });
    }
    if (ids.includes('85')) {
      callESA(req, 'request', nino, 'phone')
        .then(() => undefined);
    }
    const took = Date.now() - start;
    log('TOOK', took);
  });

  test('GUID', async () => {
    const res = await callGuidService(req, 'guid', '9f9f46d8b2115fc66510020863a182abf3c03b0501a1b3bde5d88f089a4d2857');
    log('GUID', res.status, JSON.stringify(res.data, undefined, '  '));
  });
});
