/* eslint-disable max-len */
const crypto = require('crypto');
const {
  KMSClient,
  SignCommand,
  VerifyCommand,
  GetPublicKeyCommand,
  DescribeKeyCommand,
  ListResourceTagsCommand,
} = require('@aws-sdk/client-kms');
const format = require('ecdsa-sig-formatter');
const jsonwebtoken = require('jsonwebtoken');

function log(tag, ...args) {
  console.log(`\x1b[36m${tag}\x1b[0m`, ...args); // eslint-disable-line no-console
}

function base64url(buff) {
  return buff.toString('base64')
    .replace(/=/g, '')
    .replace(/\+/g, '-')
    .replace(/\//g, '_');
}

function toBase64(str) {
  return base64url(Buffer.from(str));
}

async function signAndVerify(client, KeyId, SigningAlgorithm, message, useDigest) {
  let MessageType;
  let Message;
  if (useDigest || message.length > 4096) {
    MessageType = 'DIGEST';
    Message = crypto.createHash('sha256')
      .update(message)
      .digest();
  } else {
    MessageType = 'RAW';
    Message = Buffer.from(message);
  }

  const signCommand = new SignCommand({
    KeyId,
    Message,
    MessageType,
    GrantTokens: [],
    SigningAlgorithm, // || 'RSASSA_PSS_SHA_384' || 'RSASSA_PSS_SHA_512' || 'RSASSA_PKCS1_V1_5_SHA_256' || 'RSASSA_PKCS1_V1_5_SHA_384' || 'RSASSA_PKCS1_V1_5_SHA_512' || 'ECDSA_SHA_256' || 'ECDSA_SHA_384' || 'ECDSA_SHA_512' || 'SM2DSA', // required
    // DryRun: true || false,
  });
  const response = await client.send(signCommand);
  log('SIGN', useDigest, response);
  const buff = Buffer.from(response.Signature);
  const str = buff.toString('base64');
  log('SIGNATURE', str);
  // KeyId: 'arn:aws:kms:eu-west-2:825893415415:key/a4058d78-801a-4d04-a7a7-11b55c8c9aa8',
  // SigningAlgorithm: 'RSASSA_PSS_SHA_256'

  // now verify
  // response.Signature[42] = 42; // this will break it
  const verifyCommand = new VerifyCommand({
    KeyId,
    Message,
    MessageType,
    GrantTokens: [],
    SigningAlgorithm,
    Signature: response.Signature,
  });
  try {
    const verifyResponse = await client.send(verifyCommand);
    log('VERIFY', verifyResponse);
    // SignatureValid: true,
    // SigningAlgorithm: 'RSASSA_PSS_SHA_256'
    expect(verifyResponse.SignatureValid)
      .toEqual(true);
  } catch (e) {
    log('VERIFY ERROR', e);
    throw new Error('VERIFY ERROR');
  }
  return Buffer.from(response.Signature);
}

describe('AWS Signing', () => {
  const client = new KMSClient({
    region: 'eu-west-2',
  });
  // const KeyId = '009547f6-0c7f-4c85-b86d-efbeae0f8153';
  const EccKeyId = 'dbd46d36-d4fd-4589-8c09-879f459e75b0';
  const KeyId = 'a4058d78-801a-4d04-a7a7-11b55c8c9aa8';

  test('KMS sign and verify PS256', async () => {
    await signAndVerify(client, KeyId, 'RSASSA_PSS_SHA_256', 'This is the message');
  });

  test('KMS sign and verify RSA256', async () => {
    await signAndVerify(client, KeyId, 'RSASSA_PKCS1_V1_5_SHA_256', 'This is the message');
  });

  test('KMS sign and verify ES256', async () => {
    await signAndVerify(client, EccKeyId, 'ECDSA_SHA_256', 'This is another message');
  });

  function getJwtDataString() {
    const iat = Math.round(Date.now() / 1000);
    return JSON.stringify({
      iss: 'https://access.test.shefcon-dev.dwpcloud.uk/am/invalid-url',
      iat,
      exp: iat + 3600,
      tokenType: 'JWTToken',
      sub: 'urn:fdc:gov.uk:2022:56P4CMsGh_02YOlWpd8PAOI-2sVlB2nsNU7mcLZYhYw=',
      nonce: 'aad0aa969c156b2dfa685f885fac7083',
      aud: 'YOUR_CLIENT_ID',
      sid: 'LeiKvyPXZorTmSgOTsYhPehsGNw=',
      customClaim: {
        foo: true,
        bar: Math.random() * 10000,
        favorites: ['red', 'green', 42],
      },
    });
  }

  test('KMS sign and verify JWT token RS256', async () => {
    // const kid = 'arn:aws:kms:eu-west-2:825893415415:alias/squad-x';
    // const s = 'arn:aws:kms:eu-west-2:825893415415:key/9296a33f-84f3-4520-a9af-ebb4f17a628c';
    const kid = 'arn:aws:kms:eu-west-2:825893415415:alias/SRA_DAM_CURRENT_KEY';
    const header = JSON.stringify({
      typ: 'JWT',
      kid: KeyId,
      alg: 'RS256',
    });
    const payload = getJwtDataString();
    const message = `${toBase64(header)}.${toBase64(payload)}`;
    const sigBuffer = await signAndVerify(client, kid, 'RSASSA_PKCS1_V1_5_SHA_256', message, true);

    const sigJwt = base64url(sigBuffer);
    const JWT = `${message}.${sigJwt}`;
    log('RS256_JWT', JWT);

    const jwtDecoded = jsonwebtoken.decode(JWT, { complete: true });
    log('DECODED JWT', jwtDecoded);
  });

  test('KMS sign and verify JWT token PS256', async () => {
    const header = JSON.stringify({
      typ: 'JWT',
      kid: KeyId,
      alg: 'PS256',
    });
    const payload = getJwtDataString();
    const message = `${toBase64(header)}.${toBase64(payload)}`;
    const sigBuffer = await signAndVerify(client, KeyId, 'RSASSA_PSS_SHA_256', message, false);

    const sigJwt = base64url(sigBuffer);
    const JWT = `${message}.${sigJwt}`;
    log('PS256_JWT', JWT);

    const jwtDecoded = jsonwebtoken.decode(JWT, { complete: true });
    log('DECODED JWT', jwtDecoded);
  });

  test('KMS sign and verify JWT token ES256', async () => {
    const header = JSON.stringify({
      typ: 'JWT',
      kid: EccKeyId,
      alg: 'ES256',
    });
    const payload = getJwtDataString();
    const message = `${toBase64(header)}.${toBase64(payload)}`;
    const sigBuffer = await signAndVerify(client, EccKeyId, 'ECDSA_SHA_256', message, true);

    const sigJwt = format.derToJose(sigBuffer, 'ES256');
    const JWT = `${message}.${sigJwt}`;
    log('ES256_JWT', JWT);

    const check = format.joseToDer(sigJwt, 'ES256');
    expect(check)
      .toEqual(sigBuffer);

    const jwtDecoded = jsonwebtoken.decode(JWT, { complete: true });
    log('DECODED JWT', jwtDecoded);
  });

  test('KMS get public key', async () => {
    const start = Date.now();
    const kid = 'arn:aws:kms:eu-west-2:825893415415:alias/SRA_DAM_CURRENT_KEY';
    // const kid = 'a4058d78-801a-4d04-a7a7-11b55c8c9aa8';
    const command = new GetPublicKeyCommand({ // GetPublicKeyRequest
      KeyId: kid,
      // GrantTokens: [],
    });
    const response = await client.send(command);
    log('PUBLIC KEY', response);
    const buff = Buffer.from(response.PublicKey);
    const str = buff.toString('base64');
    log('VALUE', str);
    // format it
    const a = ['-----BEGIN PUBLIC KEY-----'];
    for (let n = 0; n < str.length; n += 64) {
      a.push(str.substring(n, n + 64));
    }
    a.push('-----END PUBLIC KEY-----');
    log('FORMATTED KEY\n', a.join('\n'));

    const scribed = await client.send(new DescribeKeyCommand({
      KeyId: kid,
    }));
    log('DESCRIBE KEY', scribed);

    const list = await client.send(new ListResourceTagsCommand({
      KeyId: scribed.KeyMetadata.KeyId,
    }));
    const end = Date.now();
    log('KEY TAGS', list);
    log('TOOK', `${end - start}ms`);
  });

  test('foo', () => {
    const s = 'arn:aws:kms:eu-west-2:825893415415:key/9296a33f-84f3-4520-a9af-ebb4f17a628c';
    const z = s.substring(s.lastIndexOf('/') + 1);
    log('kid', z);
  });
});

/* public key
      CustomerMasterKeySpec: 'RSA_2048',
      EncryptionAlgorithms: [ 'RSAES_OAEP_SHA_1', 'RSAES_OAEP_SHA_256' ],
      KeyId: 'arn:aws:kms:eu-west-2:825893415415:key/a4058d78-801a-4d04-a7a7-11b55c8c9aa8',
      KeySpec: 'RSA_2048',
      KeyUsage: 'ENCRYPT_DECRYPT', // or 'SIGN_VERIFY'

-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA06kcuRg3B/dXivaaM2K5
C7v8GTLJ0NFFmrpy3EF6AtKpMQA/uBeZ3GPuaY3xm9uURpvphOAhzHcBpJDJe6F+
whB4/EB0XoXV9TjsHG5mcOA0wCIsiMoemtPt2UIMQ/Glkfj/KH73nlaPQkug+yTg
XS2IgoenJbi3KMTjCzUz1qBysfWT7p165MO9DCPX/D3gtheeAGj4M4MiK0lHxmiD
dBbzi6w+gAf/GiFJVTCfsvRBF2nrnWvBdiPVGCmjaML20qtIVqLeYFP+oImnTYFs
LmXKWVjYlGWRbH97Ym0itwlaMXwH2P9rcipLD0OnwbFc54BrnHjNu+WnqKtv3Loh
FwIDAQAB
-----END PUBLIC KEY-----

-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAE9Svgo0XTLhNzEQZpBXax/czYTl11
KGaCE9W8ehQDPY2ZwQaWvwH2/J1Kw25PYaRoysuKLFVcVoOKt18DvojYFA==
-----END PUBLIC KEY-----
 */

// see also
// https://moneyforward-dev.jp/entry/2022/04/13/jwt-with-aws-kms-keys/
// https://aws.amazon.com/blogs/security/how-to-verify-aws-kms-signatures-in-decoupled-architectures-at-scale/
// https://medium.com/@adisesha/jwt-signature-with-aws-kms-3cb69a97fcdd

// https://docs.aws.amazon.com/AWSJavaScriptSDK/v3/latest/client/kms/command/GetPublicKeyCommand/
// https://docs.aws.amazon.com/AWSJavaScriptSDK/v3/latest/client/kms/command/VerifyCommand/

// DER
// https://stackoverflow.com/questions/71413612/kms-generated-signature-is-too-large
// https://github.com/Brightspace/node-ecdsa-sig-formatter
