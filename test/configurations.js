// const HOST_72 = 'https://am.vault.fr.dev.shefcon-dev.dwpcloud.uk:8443';
// host: 'https://am.frupg.fr.dev.shefcon-dev.dwpcloud.uk:8443', // 7.2
const HOST_72 = 'https://am.upgrade.dev.shefcon-dev.dwpcloud.uk:8443';

const LOCAL_TIDV = {
  host: 'https://forgerock.local.lvh.me',
  realm: 'TIDV',
  clientId: 'CxP-PIP-TIDV',
  secret: 'CxPTIDVClientSecret',
  redirect: 'http://localhost:3001/payments/pip',
  acr: 'TIDV-Auth-IDV-Medium',
  scope: 'openid+guid+get:benefit+create:voiceprint+get:idv-failure+update:personal-details',
};

const UPGRADE_TIDV = {
  ...LOCAL_TIDV,
  host: HOST_72,
  // redirect: 'https://benefits-frupg.dev.shefcon-dev.dwpcloud.uk/payments/pip',
  redirect: 'https://benefits-vault.dev.shefcon-dev.dwpcloud.uk/payments/pip',
  scope: 'openid+guid+get:benefit+create:voiceprint+get:idv-failure+update:personal-details',
};

const TEST_TIDV = {
  ...UPGRADE_TIDV,
  host: 'https://am.fr.test.shefcon-dev.dwpcloud.uk:8443', // TEST
  redirect: 'https://benefits.test.shefcon-dev.dwpcloud.uk/payments/pip',
};

const DEV_TIDV = {
  ...UPGRADE_TIDV,
  host: 'https://am.fr.dev.shefcon-dev.dwpcloud.uk:8443', // TEST
  redirect: 'https://benefits.dev.shefcon-dev.dwpcloud.uk/payments/pip',
};

const LOCAL_WEB = {
  host: 'https://forgerock.local.lvh.me',
  realm: 'WEB',
  clientId: 'RMD-WEB',
  secret: 'RMDClientSecret',
  redirect: 'http://localhost:3002/auth/callback',
  acr: 'Auth-Level-Medium', // NOTE YOU HAVE TO ADD THIS
  scope: 'openid+guid',
};

const configurations = {
  LOCAL_WEB,
  UPGRADE_WEB: {
    host: HOST_72,
    realm: 'WEB',
    clientId: 'RMD-WEB',
    secret: 'RMDClientSecret',
    // redirect: 'https://rmd.frupg.stubs.dev.shefcon-dev.dwpcloud.uk/auth/callback',
    redirect: 'http://localhost:3002/auth/callback',
    acr: 'Auth-Level-Medium',
    scope: 'openid+guid+email+phone',
  },
  LOCAL_TIDV,
  UPGRADE_TIDV,
  TEST_TIDV,
  DEV_TIDV,
  MOCK_WEB: {
    ...LOCAL_WEB,
    host: 'http://localhost:8080',
  },
};

module.exports = configurations;
