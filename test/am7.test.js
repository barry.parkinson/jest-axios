/* eslint-disable max-len */
const { callAuthorize, callAMTree, startAMRecording } = require('./oidc-helpers');
const { UPGRADE_TIDV: config } = require('./configurations');

function log(tag, ...args) {
  process.stdout.write(`\x1b[36m${tag}\x1b[0m ${args.join(' ')}\n`);
}

describe('AM7', () => {
  test('Retrieve', async () => {
    const res = await callAMTree({}, config, 'TEST_TREE');
    log('TEST_TREE', res.status, JSON.stringify(res.data));
  });

  test('Start Recording', async () => {
    await startAMRecording(config);
  });

  test('Matching', async () => {
    const res = await callAMTree({
      // 'x-dwp-guid': 'ed358979c05c838c3e6dfd088ca84a2f148467a79a9e504936d8f167464bf661',
      'x-dwp-guid': 'dbfba70f83735c0a21f4ad49094b37c1e05ae5ecf500e5c2d4296b46f69b1da3',
      //  'x-dwp-guid': 'e514c436e4215832d3676e19b2e939d25546599fd9ade37450d3981459013bdb',
    }, config, 'TIDV_MATCH');
    log('TIDV_MATCH', res.status, JSON.stringify(res.data));
  });

  test('Sign In', async () => {
    const res = await callAMTree({
      // 'x-dwp-guid': 'ed358979c05c838c3e6dfd088ca84a2f148467a79a9e504936d8f167464bf661',
      'x-dwp-guid': 'dbfba70f83735c0a21f4ad49094b37c1e05ae5ecf500e5c2d4296b46f69b1da3',
      // 'x-dwp-guid': '1000000000000832d3676e19b2e939d25546599fd9ade37450d3981459013bdb', // invalid guid
      'x-auth-level': 2,
      'x-success-url': '/session/12345678',
      'x-allow-update': 'no',
    }, config, 'TIDV_SIGN_IN');
    log('TIDV_SIGN_IN', res.status, JSON.stringify(res.data));
    await callAuthorize(res.data.tokenId, config);
  });
});
