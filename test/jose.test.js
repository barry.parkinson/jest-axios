const jose = require('jose');
const crypto = require('crypto');
const axios = require('axios');
// eslint-disable-next-line import/no-unresolved
const { JWSSignatureVerificationFailed } = require('jose/errors');
const generateKeyPair = require('../app/generateKeyPair');

function log(tag, ...args) {
  process.stdout.write(`\x1b[36m${tag}\x1b[0m ${args.join(' ')}\n`);
}

describe('jose tests', () => {
  jest.setTimeout(20000);

  test('decrypt jwt', async () => {
    const secret = jose.base64url.decode('zH4NRP1HMALxxCFnRZABFA7GOJtzU_gIj02alfL1lvI');
    const jwt = 'eyJhbGciOiJkaXIiLCJlbmMiOiJBMTI4Q0JDLUhTMjU2In0..MB66qstZBPxAXKdsjet_lA.WHbtJTl4taHp7otOHLq3hBvv0yNPsPEKHYInmCPdDDeyV1kU-f-tGEiU4FxlSqkqAT2hVs8_wMNiQFAzPU1PUgIqWCPsBrPP3TtxYsrtwagpn4SvCsUsx0Mhw9ZhliAO8CLmCBQkqr_T9AcYsz5uZw.7nX9m7BGUu_u1p1qFHzyIg';

    const {
      payload,
      protectedHeader,
    } = await jose.jwtDecrypt(jwt, secret, {
      issuer: 'urn:example:issuer',
      audience: 'urn:example:audience',
    });

    log('HEADER', JSON.stringify(protectedHeader));
    log('PAYLOAD', JSON.stringify(payload));
  });

  function splitJwe(jwe) {
    const {
      0: aad,
      1: encryptedKey,
      2: iv,
      3: ciphertext,
      4: authTag,
    } = jwe.split('.');
    return {
      aad, encryptedKey, iv, ciphertext, authTag,
    };
  }

  const publicKeyData = `-----BEGIN PUBLIC KEY-----
MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAsqkRDposiv0OReD+p/OJ
p0y+gsbVA1BDOwMLtVn6tsuzCRzmWKPIjIP9IMmUklB7hYWFJxYowijZTrOcDrSj
3T6YIshXHQfkUO1RiW2Bh1KphcgLUdACec8fG3/PItZ8Zb0YF5jwTM6OfsfFguPi
KDeudsJYvp2NMWZvZcKjBTQIIBvpKZuUqy/LwV3kTTddRxRPI7a5Non3eo/xO7Jp
7xt/D/kbJTimhCcJTDfSvw/HUyhyin7MtqDaX082E4QHgzLLdSM1ltTXyxR0g/mz
tP4BHSxAcNX5DqrX/xBN0mi/SflXrtEdusWBYXbCRz7gvnVETFtCAQckLq+h4eu2
DluBas5G1LWvd/h0qFdJBnGiImO+a4fjnzA3eVAgvRMPJyYKPlWxtx1dqsPAwNJB
KTBjQRzqzZi7BDVl/b+xAYRvPiWp934ZUqmRazXV8LGdWrU+wLibjDOOcRso2YAI
aUAYsPE+1Qh5hCxmwgx5W2ONexShQxyOQQPBGI25703K0k4XNORju7331QVWSOWn
QEEeeUkVRdY0IhqJZSlzRhOHB5qx6upaDvaj+cJvVWQZWmoEpegXaRx/M2jumjhf
L9BDe4EudG2XSCcWrzYiloPqY3ZEeYgeVlG4QacnnDqGMQOGVqxZUUYM5zDbznw2
B8Og9539gwHrdE/7TmdpEykCAwEAAQ==
-----END PUBLIC KEY-----`;

  test('encrypt and decrypt with service', async () => {
    // get the key to encrypt
    const publicKey = crypto.createPublicKey(publicKeyData);
    const jwk = publicKey.export({ format: 'jwk' });
    // jwk.kid = '088496a8-a809-472f-abdb-18c1732daebb';
    log('JWK', JSON.stringify(jwk, undefined, ' '));
    const key = await jose.importJWK(jwk);
    // alternative...
    // const key = await jose.importSPKI(publicKeyData, 'RS256');

    // the payload to encrypt
    const jws = `Something you expect here ${new Date()}`;

    // encrypt it
    const jwe = await new jose.CompactEncrypt(new TextEncoder().encode(jws))
      .setProtectedHeader({ alg: 'RSA-OAEP-256', enc: 'A256GCM' })
      .encrypt(key);
    log('JWE', jwe);

    // now decrypt it
    const data = {
      context: 'KBV-AAS',
      ...splitJwe(jwe),
    };
    const res = await axios({
      method: 'post',
      url: 'https://kbv-asymmetric-crypto.kbv.idt.pdu-dev.hcs-eks.dwpcloud.uk/decrypt-jwe',
      data,
    });
    log('DECRYPTED', res.status, JSON.stringify(res.data));
  });

  async function makeJWT(alg, key) {
    return new jose.SignJWT({
      'urn:example:claim': true,
      ssoSessionId: 'yabba-dabba-doo',
    })
      .setProtectedHeader({ alg, kid: 'sample-key-id', typ: 'JWT' })
      .setIssuedAt()
      .setIssuer('urn:example:issuer')
      .setAudience('urn:example:audience')
      .setExpirationTime('2h')
      .sign(key);
  }

  test('Sign JWT HS256', async () => {
    const key = crypto.randomBytes(32);
    const alg = 'HS256';
    const jwt = await makeJWT(alg, key);
    log('JWT', jwt);
    log('Key:', key.toString('base64url'));
  });

  test('Sign JWT ES512', async () => {
    const alg = 'ES512';
    // eslint-disable-next-line no-use-before-define
    const key = crypto.createPrivateKey(EC_PRIVATE_512);
    const jwt = await makeJWT(alg, key);
    log('JWT', jwt);
  });

  test('Sign JWT ES256K', async () => {
    const alg = 'ES256K';
    // eslint-disable-next-line no-use-before-define
    const key = crypto.createPrivateKey(EC_PRIVATE_256K);
    const jwt = await makeJWT(alg, key);
    log('JWT', jwt);
    const pub = crypto.createPublicKey(EC_PUBLIC_256K); // eslint-disable-line no-use-before-define
    const ver = await jose.jwtVerify(jwt, pub);
    log('VER', JSON.stringify(ver, null, ' '));
    // mismatch
    // eslint-disable-next-line no-use-before-define
    const bad = crypto.createPublicKey(EC_PUBLIC_BAD_256K);
    expect(() => jose.jwtVerify(jwt, bad))
      .rejects
      .toEqual(new JWSSignatureVerificationFailed('signature verification failed'));
  });

  test('Generate EdDSA 25519 key pair', async () => {
    const keypair = crypto.generateKeyPairSync(
      'ed25519', // or ed448
      {
        privateKeyEncoding: { format: 'pem', type: 'pkcs8' },
        publicKeyEncoding: { format: 'pem', type: 'spki' },
      },
    );
    log('PRIVATE', keypair.privateKey);
    log('PUBLIC', keypair.publicKey);
    const key = crypto.createPrivateKey(keypair.privateKey);
    const token = await makeJWT('EdDSA', key);
    log('JWT:', token);
    const pub = crypto.createPublicKey(keypair.publicKey);
    const ver = await jose.jwtVerify(token, pub);
    log('VER', JSON.stringify(ver, null, ' '));
  });

  test('Generate EC key pair', async () => {
    const keypair = crypto.generateKeyPairSync('ec', {
      namedCurve: 'secp256k1', // prime256v1, secp384r1, secp521r1, secp256k1
      privateKeyEncoding: { format: 'pem', type: 'pkcs8' },
      publicKeyEncoding: { format: 'pem', type: 'spki' },
    });
    log('PRIVATE', keypair.privateKey);
    log('PUBLIC', keypair.publicKey);
    const key = crypto.createPrivateKey(keypair.privateKey);
    const token = await makeJWT('ES256K', key);
    log('JWT:', token);
  });

  test('Generate rsa key pair', async () => {
    const keypair = crypto.generateKeyPairSync('rsa', {
      modulusLength: 2048,
      privateKeyEncoding: { format: 'pem', type: 'pkcs8' },
      publicKeyEncoding: { format: 'pem', type: 'spki' },
    });
    log('PRIVATE', keypair.privateKey);
    log('PUBLIC', keypair.publicKey);
    const key = crypto.createPrivateKey(keypair.privateKey);
    const token = await makeJWT('RS256', key);
    log('JWT:', token);
  });

  test('Generate a key pair', async () => {
    // console.log(crypto.getCurves());
    const alg = 'ES256';
    const keypair = generateKeyPair(alg);
    log(`${alg}_PRIVATE:\n`, keypair.privateKey);
    log(`${alg}_PUBLIC:\n`, keypair.publicKey);
    const key = crypto.createPrivateKey(keypair.privateKey);
    const token = await makeJWT(alg, key);
    log(`${alg}_JWT:\n`, token);
    const pub = crypto.createPublicKey(keypair.publicKey);
    await jose.jwtVerify(token, pub); // throws if invalid
  });
});

const EC_PRIVATE_512 = `-----BEGIN EC PRIVATE KEY-----
MIHcAgEBBEIAwHHoo5tXoNUTSSoJwZpuo8rUsBJCHZO9p5ZZPxz+0Z0MHWirJyzg
oOWHdgcP6esH+UrJ3D7Q7ukPf6nx8nJB3WCgBwYFK4EEACOhgYkDgYYABAHSbnMv
W3lSEM9VCo6xw8yMGNupLEUw3qG7nv1X3vdYDAu7rR7S9AGgPBoY/kea/6+D22g9
wCnk6jmuZ3F4XOJzfwBjZTJizFM/5/Rjhn1TJb+AzADmT5sUPyN11wclKlPhWspB
ovtGzXhRCo4j9rpSXgzxiQd5H3ygdRKIP3kEgXMW4w==
-----END EC PRIVATE KEY-----`;

const EC_PRIVATE_256K = `-----BEGIN EC PRIVATE KEY-----
MHQCAQEEILn7U71l2XOhIDRvtB9sJIiQFn1WvcL2jzApCaq466wkoAcGBSuBBAAK
oUQDQgAElL/hHFR4C4je1lT3vSoP80hYW0wb/V1MEtlpknrWeU8BmMUjXmCuoTYZ
gxp3Ox+YloqDImR6YjqJcR+dNU7U7g==
-----END EC PRIVATE KEY-----`;

const EC_PUBLIC_256K = `-----BEGIN PUBLIC KEY-----
MFYwEAYHKoZIzj0CAQYFK4EEAAoDQgAElL/hHFR4C4je1lT3vSoP80hYW0wb/V1M
EtlpknrWeU8BmMUjXmCuoTYZgxp3Ox+YloqDImR6YjqJcR+dNU7U7g==
-----END PUBLIC KEY-----`;

const EC_PUBLIC_BAD_256K = `-----BEGIN PUBLIC KEY-----
  MFYwEAYHKoZIzj0CAQYFK4EEAAoDQgAEpxstddofWjtlijNk0ZFtKDDQM5/B6FN1
mosOzkQk0/KLuacgBiyV7JATOne8E3u7+S4nYm8u1b9QIh0vzTHwHw==
-----END PUBLIC KEY-----`;
