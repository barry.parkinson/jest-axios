const os = require('os');
const axios = require('axios');
const { v1: uuid } = require('uuid');
const { LOCAL_WEB: config, LOCAL_TIDV, LOCAL_WEB } = require('./configurations');
const {
  callAMTree,
  callAuthorize,
  OPTIONS,
  introspect, // eslint-disable-line no-unused-vars
  userInfo, // eslint-disable-line no-unused-vars
  endSession, // eslint-disable-line no-unused-vars
  sessionLogout, // eslint-disable-line no-unused-vars
} = require('./oidc-helpers');

function log(tag, ...args) {
  process.stdout.write(`\x1b[36m${tag}\x1b[0m ${args.join(' ')}\n`);
}

async function callTree(headers) {
  return callAMTree(headers, config, 'OIDV2-BASE-TREE');
}

function parseResponse(data) {
  const { detail = {} } = data;
  const { failureUrl } = detail;
  if (failureUrl) {
    const success = failureUrl.startsWith('SUCCESS: ');
    const json = success ? failureUrl.substring(9) : failureUrl;
    return {
      success: success ? true : undefined,
      ...JSON.parse(json),
    };
  }
  return {};
}

function debug(tag, res) {
  const { data } = res;
  if (data.detail && data.detail.failureUrl) {
    log(tag, JSON.stringify(parseResponse(res.data)));
  } else {
    log(tag, res.status, JSON.stringify(res.data));
  }
}

describe('forgerock', () => {
  jest.setTimeout(20000);

  beforeEach(() => {
    process.stdout.write(`\x1b[34m---- TEST ---- ${expect.getState().currentTestName} ----------\n`);
  });

  test('tree provision', async () => {
    const res = await callTree({
      'x-action': 'provision',
      'x-email': 'dave@dave.com',
      'x-password': 'FooBar123!',
      // 'x-phone': '01610001234',
      // 'x-dwp-guid': '13f03f9da3a0f493e04df091865f8e77f636ac211948e10ec94e279392bc75a5',
      // 'x-credential-id': uuid(),
      // 'x-last-logins': '{}',
      // 'x-failed-password': '0',
      // 'x-failed-otp': '0',
      // 'x-phone': '07986222222',
      'user-agent': `${os.hostname()}/provision`,
    });
    debug('PROVISION', res);
  });

  test('tree retrieve', async () => {
    const email = 'dave@dave.com';
    const res = await callTree({
      'x-action': 'retrieve',
      'x-email': email,
      'user-agent': `${os.hostname()}/retrieve`,
    });
    debug('RETRIEVE', res);
    const data = parseResponse(res.data);
    const { lastLogins, ...rest } = data;
    log(`RETRIEVE ${email}`, JSON.stringify(rest, null, ' '));
    if (data.tempLockedOut) {
      const tempLockedOut = JSON.parse(data.tempLockedOut);
      if (tempLockedOut.authentication) {
        const d = new Date(tempLockedOut.authentication.timestamp);
        log('AUTH_LOCKOUT', d);
      }
      if (tempLockedOut['password-reset']) {
        const d = new Date(tempLockedOut['password-reset'].timestamp);
        log('PASSWORD_RESET_LOCKOUT', d);
      }
    }
    if (lastLogins && lastLogins !== '{}') { // set to {} when account created
      const parsed = JSON.parse(lastLogins);
      if (Array.isArray(parsed)) {
        const mod = parsed.map(({
          date,
          status,
        }) => ({
          date: new Date(parseInt(date, 10)),
          status,
        }));
        log('lastLogins', JSON.stringify(mod, null, ' '));
      }
    }
  });

  test('tree check-password', async () => {
    const res = await callTree({
      'x-action': 'check-password',
      'x-email': 'dave@dave.com',
      'x-password': 'FooBar123!',
      'user-agent': `${os.hostname()}/check-password`,
    });
    debug('TREE-CP', res);
  });

  test('tree update password', async () => {
    const res = await callTree({
      'x-action': 'update',
      'x-email': 'dave@dave.com',
      'x-password': 'FooBar123!',
      // 'x-dwp-guid': '13f03f9da3a0f493e04df091865f8e77f636ac211948e10ec94e279392bc75a5',
      // 'x-credential-id': uuid(),
      'x-phone': '079861231239',
      // 'x-last-reset': '[]',
      'user-agent': `${os.hostname()}/update-password`,
    });
    debug('TREE-UPDATE-PASS', res);
  });

  test('tree update reset credential id', async () => {
    const res = await callTree({
      'x-action': 'update',
      'x-email': 'bob@bob.com',
      // 'x-dwp-guid': '-',
      'x-credential-id': uuid(),
      'x-user-status': 'Active',
      'x-failed-otp': 0,
      'x-failed-password': 0,
      'x-temp-locked': '{}',
      'x-last-logins': JSON.stringify([{ date: 1661500336021, status: 'SUCCESS' }]),
      // 'x-last-logins': '[]',
      'x-last-reset': JSON.stringify([{ date: 1661500336021 }]),
      'user-agent': `${os.hostname()}/update-cid`,
    });
    debug('TREE-UPDATE-GENERAL', res);
  });

  test('tree check-password updated', async () => {
    const res = await callTree({
      'x-action': 'check-password',
      'x-email': 'scoby@gmail.com',
      'x-password': '123NewPass',
      'user-agent': `${os.hostname()}/check-password-u`,
    });
    debug('TREE-CP-UPDATED', res);
  });

  test('tree authenticate', async () => {
    const res = await callTree({
      'x-action': 'authenticate',
      'x-email': 'dave@dave.com',
      'x-auth-level': '2',
      'x-success-url': 'SUCCESS',
      'user-agent': `${os.hostname()}/authenticate`,
    });
    // res.data.tokenId should be present
    log('TREE-AUTH', res.status, JSON.stringify(res.data));
    const tok = await callAuthorize(res.data.tokenId, config); // eslint-disable-line no-unused-vars
    // await endSession(tok.id_token, config);
    // await sessionLogout(res.data.tokenId, config);
    // await introspect(tok.access_token, config);
    // await userInfo(tok.access_token, config);
    // await callAuthorize(res.data.tokenId, config); // this will fail, redirect to authenticate
  });

  test('authenticate WEB with idt provider', async () => {
    const mod = {
      ...LOCAL_WEB,
      host: 'http://localhost:4044',
    };
    const res = await axios({
      url: `${mod.host}/sso/create-token`,
      method: 'post',
      ...OPTIONS,
      data: {
        subject: '91638f27-9a81-4279-8bc2-9874d0615c67',
        authLevel: 2,
        acr: 'Auth-Level-Medium',
        clientId: 'RMD-WEB',
        realm: '/Citizens/WEB',
        scopes: [
          'openid',
          'guid',
          'email',
        ],
        claims: {
          guid: 'f51cbaa1abd28e74c239a25168a090d9ccf38ed80cb349e9a7fc18df239ac701',
          email: 'test873@example.com',
          vot: 'P2.Cl.Cm',
        },
        authenticationSessionId: 'f3aec1ed-73bc-4606-bda6-94e02b396377',
      },
    });
    log('SSO-TOKEN', res.status, JSON.stringify(res.data));
    await callAuthorize(res.data.tokenId, mod);
  });

  test('authenticate TIDV with idt provider', async () => {
    const mod = {
      ...LOCAL_TIDV,
      host: 'http://localhost:4044',
    };
    const res = await axios({
      url: `${mod.host}/sso/create-token`,
      method: 'post',
      ...OPTIONS,
      data: {
        subject: '7a238f27-9a81-4279-8bc2-9874d0615c67',
        authLevel: 1,
        acr: 'TIDV-Auth-IDV-Medium',
        clientId: 'CxP-PIP-TIDV',
        realm: '/Citizens/TIDV',
        scopes: [
          'openid',
          'guid',
          'get:idv-failure',
          'get:benefit',
          // 'create:voiceprint',
          'update:personal-details',
        ],
        claims: {
          guid: 'f51cbaa1abd28e74c239a25168a090d9ccf38ed80cb349e9a7fc18df239ac701',
          credentialId: '5168282b-09a9-4b60-b9b6-ef2d51da136d',
        },
        authenticationSessionId: '24aec1ed-73bc-4606-bda6-94e02b396377',
      },
    });
    log('SSO-TOKEN TIDV', res.status, JSON.stringify(res.data));
    await callAuthorize(res.data.tokenId, mod);
  });

  // expected failure tests

  test('tree update fails with unknown user', async () => {
    const res = await callTree({
      'x-action': 'update',
      'x-email': 'unknown@example.com',
      'x-password': 'xxxYYY123!!!',
      'user-agent': `${os.hostname()}/update-f`,
    });
    debug('TREE-UP-FAIL', res);
  });

  test('tree check-password on unknown user', async () => {
    const res = await callTree({
      'x-action': 'check-password',
      'x-email': 'unknown@example.com',
      'x-password': 'xxxYYY123!!!',
      'user-agent': `${os.hostname()}/check-password-f`,
    });
    debug('TREE-PASS-FAIL', res);
  });

  test('register', async () => {
    // SEE https://openid.net/specs/openid-connect-registration-1_0.html#RegistrationRequest
    const url = 'https://forgerock.local.lvh.me/am/oauth2/realms/root/realms/Citizens/realms/TIDV/register';
    // const url = 'https://am.upgrade.dev.shefcon-dev.dwpcloud.uk:8443/am/oauth2/realms/root/realms/Citizens/realms/TIDV/register';
    const res = await axios({
      url,
      method: 'post',
      ...OPTIONS,
      data: {
        redirect_uris: ['http://localhost:3002/foobar'],
        client_name: 'CxP-PCC-TIDV',
        subject_type: 'public', // or 'pairwise',
        token_endpoint_auth_method: 'client_secret_post', // 'client_secret_basic',
        contacts: ['ve7jtb@example.org', 'mary@example.org'],
        backchannel_logout_session_required: true,
      },
    });
    log('REGISTER', res.status, JSON.stringify(res.data, undefined, ' '));
  });
});

/* EXAMPLE
{
 "inetuserstatus": "Active",
 "telephoneNumber": "07986315960",
 "dwpGuid": "13f03f9da3a0f493e04df091865f8e77f636ac211948e10ec94e279392bc75a5",
 "credentialId": "dc81e930-c717-11ed-9683-b74947ab5c7f",
 "failedPasswordEntries": "0",
 "failedOTPEntries": "0",
 "tempLockedOut": "{\"authenticate\":{\"timestamp\":1679395840809,\"numberOfLockouts\":1}}",
 "createTimestamp": "20230320120801Z"
}

"tempLockedOut": "{\"password-reset\":{\"timestamp\":1679400641901}}",
"tempLockedOut": "{\"account-recovery\":{\"timestamp\":1679401126689}}",
 */
