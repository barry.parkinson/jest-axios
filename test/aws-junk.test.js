/* eslint-disable max-len */
const crypto = require('crypto');
const {
  KmsKeyringNode,
  buildClient,
  CommitmentPolicy,
} = require('@aws-crypto/client-node');

// NODE_TLS_REJECT_UNAUTHORIZED=0

describe('AWS encrypt', () => {
  test('encrypt and decrypt', async () => {
    const { encrypt, decrypt } = buildClient(
      CommitmentPolicy.REQUIRE_ENCRYPT_REQUIRE_DECRYPT,
    );

    // A KMS CMK is required to generate the data key. You need kms:GenerateDataKey permission on the CMK in generatorKeyId.
    const generatorKeyId = 'arn:aws:kms:eu-west-2:825893415415:alias/squad-x-test-encryption-key';
    // Adding alternate KMS keys that can decrypt. Access to kms:Encrypt is required for every CMK in keyIds.
    const keyIds = [
      'arn:aws:kms:eu-west-2:825893415415:key/27028cda-8eb5-497c-a763-290cef35ff60',
    ];
    const keyring = new KmsKeyringNode({ generatorKeyId, keyIds });
    const context = {
      stage: 'demo',
      purpose: 'simple demonstration app',
      origin: 'us-west-2',
    };

    // encrypt
    const cleartext = 'asdf';
    const { result } = await encrypt(keyring, cleartext, {
      encryptionContext: context,
    });
    console.log(result); // eslint-disable-line no-console

    // decrypt
    const { plaintext, messageHeader } = await decrypt(keyring, result);
    console.log(plaintext, messageHeader); // eslint-disable-line no-console

    // Grab the encryption context so you can verify it.
    // const { encryptionContext } = messageHeader;
  });

  test('basic encrypt', () => {
    const plain = `The rain in Spain is mainly... ${crypto.randomUUID()}`;
    const publicKey = '-----BEGIN PUBLIC KEY-----\n'
      + 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyJM7JQjmWcoCteY65S4o\n'
      + 'ALtNvh0QAhD9m3khQzY06y1cXkDVbZWRYVXjOD9XIEOFZ+55nSDMcVQ1b4p4K6cw\n'
      + 'e4zFvXkFPxDCy4YO00aAq0PZgyhbyKhogdYbdTIGCf0Szbh7juYZMhM0n/6WaTQl\n'
      + 'J0beEqojoUhWSVBdDl6Q//U11LXwSwBU6zbXuYAonr1IUCaXhuk73FQqNJM30jJ3\n'
      + '5t+xZa1ZRgtjoi+KxVile0yemqZLa7VnfI3b8q9YNnh6/+nCe1IXmj5vXOjl8DyK\n'
      + 'KjEd0TWMmyC/1z93m28cl5fbaCnx4KUsiA8XQRayZFhR9ZAO1wbzPO3qcs2B7WBX\n'
      + 'NQIDAQAB\n'
      + '-----END PUBLIC KEY-----';
    const buffer = Buffer.from(plain);
    const encrypted = crypto.publicEncrypt(publicKey, buffer);
    console.log(encrypted.toString('base64')); // eslint-disable-line no-console

    const privateKey = '-----BEGIN PRIVATE KEY-----\n'
      + 'MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDIkzslCOZZygK1\n'
      + '5jrlLigAu02+HRACEP2beSFDNjTrLVxeQNVtlZFhVeM4P1cgQ4Vn7nmdIMxxVDVv\n'
      + 'ingrpzB7jMW9eQU/EMLLhg7TRoCrQ9mDKFvIqGiB1ht1MgYJ/RLNuHuO5hkyEzSf\n'
      + '/pZpNCUnRt4SqiOhSFZJUF0OXpD/9TXUtfBLAFTrNte5gCievUhQJpeG6TvcVCo0\n'
      + 'kzfSMnfm37FlrVlGC2OiL4rFWKV7TJ6apktrtWd8jdvyr1g2eHr/6cJ7UheaPm9c\n'
      + '6OXwPIoqMR3RNYybIL/XP3ebbxyXl9toKfHgpSyIDxdBFrJkWFH1kA7XBvM87epy\n'
      + 'zYHtYFc1AgMBAAECggEBAJCwzVDJG8sYFF5+CGx9oHPuqoWD16F1+k0m05z1xLUG\n'
      + '0wRXRt+3rtmFqNVRtBtdOiagfPCh0XnLooSlYk0zXaEzCw7E98jn4W4KdjVpPbbO\n'
      + 'ymF4QsX83u8SsloXzQ11mBP5ioCqcvCRtDva30xjRo4P7HGQdQWK9lwK8iZEh9ts\n'
      + 'FMLVyoKnP6cnzM0fgb9Ycg30cSQNTCuOUD25mU2nAAiLcEVmrB0fq5W+Qr/uyp5z\n'
      + 'Oyn4TuTUVY6RvQG+ZN+AbyvMk1TeOsXzZyHobbbgg7MT9myq8P5wrsNds88wj+jS\n'
      + 'JtTmmcCSjJcBhDCZkU8HJSW1KjIuMGAdfQMMaOmr/0ECgYEA7DudiMFCL2KwWGoL\n'
      + 'pRaqagXAQNXQr06CXwO6kjsrk4SVcEdDSaHplyIu8vi3kVNTFed+IgBRWTNRoDPk\n'
      + 'a0ZDM5Lo1zFdaYqzCPBcRw+NJKLHtrG7eu/pzQ7b4pG0AGATgk5gudZ51XiV/sns\n'
      + 'gC11lRw7pCsrmlo6wEJc0yKIC2UCgYEA2VvI7v6iqhOw7x96PPA3o4KANH/cfcGA\n'
      + 'hy9XgF3ZclUc8YrZDulGp3iA5tLBhZSlp2gX0gRVVKMM8SWm4A/wD+qb3vvdUm8u\n'
      + 'c/Wmwqb40cfDoOV/YKByN/2DEwK+YVon3nKZScMBTwx+h/O/I4VDi1M6ajWM1AYm\n'
      + '5Ckv9XCnp5ECgYEAmdjFYr/B08xo3lvJ060sNLokF9VoABOYMjriccOZQIxTKnku\n'
      + 'qDCLSUMY4mH6YumqLZTCf3BHilkyqqvA8eAjed4OR789f7O1n3eyERFesWJVwHSF\n'
      + 'ja3bTRGFQEv1t1zat71FnyG/3LeYsANfQ6bXcdGeQe9fiTmEE5vkihhuE0UCgYEA\n'
      + 'jMg2TRYApcg7QaZRtkagwvczss1eMtWyOd0f/7TJspQOOySDknTlnnMeimLXlX3C\n'
      + 'DbwOMak826UALPEGNPodszFx4+ueXlZE8Dab3FfpsXaB7yS2bA9/62P4EcglJ0kN\n'
      + 'qbbwMiZsIpclHUly877NEODHZSNqI6oYZo+G0KI29QECgYBA7LrlFywea8tzxEhE\n'
      + 'dXcXFYTwDdNMmwkOC7ZS+hLObsucEY2KKXWFR/wLh1oeYDo9aVXlZX9GkmZaa4mz\n'
      + 'EvBJ5XJs2tuzGCwOrH73AIPtloC7eFq/y8fUiH/jirQ0+xVGMQLM2BjSSohAEhGR\n'
      + 'R+TqnpuzFG2txZwx57AqwY6r5Q==\n'
      + '-----END PRIVATE KEY-----\n';
    const decrypted = crypto.privateDecrypt(privateKey, encrypted);
    console.log(decrypted.toString('utf8')); // eslint-disable-line no-console
  });

  test('jose jwk', async () => {
    // const { generateKeyPair } = require('jose');
    // const { fromKeyLike } = require('jose');
    // const { privateKey } = await generateKeyPair('RS256');
    // const jwk = await fromKeyLike(privateKey);
    // console.log(JSON.stringify(jwk));

    const QpublicKeyData = 'MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEv/3sJqvBwKVG1IP8wYQVSJOKT3nIbMNBpYWhacecmXBkKl/PzAtSt/3A/LSioWVOotSr2A7Nuu0kq54euUW7PQ==';

    const publicKeyData = '-----BEGIN PUBLIC KEY-----\n'
      + 'MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEv/3sJqvBwKVG1IP8wYQVSJOKT3nI\n'
      + 'bMNBpYWhacecmXBkKl/PzAtSt/3A/LSioWVOotSr2A7Nuu0kq54euUW7PQ==\n'
      + '-----END PUBLIC KEY-----\n';

    const ZpublicKeyData = '-----BEGIN PUBLIC KEY-----\n'
      + 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyJM7JQjmWcoCteY65S4o\n'
      + 'ALtNvh0QAhD9m3khQzY06y1cXkDVbZWRYVXjOD9XIEOFZ+55nSDMcVQ1b4p4K6cw\n'
      + 'e4zFvXkFPxDCy4YO00aAq0PZgyhbyKhogdYbdTIGCf0Szbh7juYZMhM0n/6WaTQl\n'
      + 'J0beEqojoUhWSVBdDl6Q//U11LXwSwBU6zbXuYAonr1IUCaXhuk73FQqNJM30jJ3\n'
      + '5t+xZa1ZRgtjoi+KxVile0yemqZLa7VnfI3b8q9YNnh6/+nCe1IXmj5vXOjl8DyK\n'
      + 'KjEd0TWMmyC/1z93m28cl5fbaCnx4KUsiA8XQRayZFhR9ZAO1wbzPO3qcs2B7WBX\n'
      + 'NQIDAQAB\n'
      + '-----END PUBLIC KEY-----';
    const publicKey = crypto.createPublicKey(publicKeyData);
    const jwk = publicKey.export({ format: 'jwk' });
    console.log(jwk);
  });
});

// https://github.com/aws/aws-encryption-sdk-javascript/blob/master/modules/example-node/src/kms_simple.ts
